import axios from "axios";
import { store } from "../redux/store";
import { setCurrentUser } from "../redux/user/user.action";
import { setToken } from "../redux/token/token.action";

axios.interceptors.request.use(async (config) => {
  try {
    const state = store.getState();
    const authToken = state.token;
    if (authToken) {
      config.headers.Authorization = "Bearer " + authToken;
    }
    config.headers['Content-Type']= 'application/json';
    config.headers['Accept']= 'application/json';
    return config;
  } catch (err) {
    Promise.reject(err);
  }
});

axios.interceptors.response.use(
  (res) => {
    // if (res.data.toast === true) {
    //   store.dispatch(
    //     setErrorNotification({
    //       message: res.data.message,
    //       toggle: res.data.toast,
    //       type: res.data.response_type,
    //     })
    //   );
    //   setTimeout(() => {
    //     store.dispatch(
    //       setErrorNotification({
    //         message: "",
    //         toggle: false,
    //         type: "",
    //       })
    //     );
    //   }, 10000);
    // }
    return res.data;
  },
  (e) => {
    console.log(e);
    if (e.response.status === 401) {
      store.dispatch(setCurrentUser(null));
      store.dispatch(setToken(null));
      window.location.reload();
    }
  }
);

export default axios;

export const axiosPost = (url, data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}${url}`, data);
};

export const axiosGet = (url, data = {}) => {
  return axios.get(`${process.env.REACT_APP_API_URL}${url}`, {
    params: data,
  });
};
