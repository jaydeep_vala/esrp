import React, { Suspense } from "react";
import { Switch, Route } from "react-router-dom";
// import Dashboard from "./components/dashboard/dashboard.component";
// import Linechart from "./components/charts/line/linechart.component";
// import Barchart from "./components/charts/bar/barchart.component";
// import Informer from "./components/charts/informer/informer.component";
// import DonutChart from "./components/charts/donut/donut.component";
// import ChartTable from "./components/charts/table/table.component";
// import Header from "./components/header/header.component";
// import ThemeChange from "./components/theme-change/ThemeChange-component";
// import Sidebar from "./components/sidebar/Sidebar.component";
// import Loading from "./components/loading/loading-component";
// import Footer from "./components/footer/footer.component";
// import ButtonCompo from "./components/buttons/button.component";
// import CircleChart from "./components/charts/circle/circle.component";
import "./App.css";
// import BubbleChart from "./components/charts/bubble/bubble.component";
// import ColumnChart from "./components/charts/column/column.component";
// import TimelineChart from "./components/charts/timeline/timeline.component";
// import DotChart from "./components/charts/dot/dot.component";
// import AnalysisDashboard from './components/wageAnalysis/analysis-dashboard.component';
// import AnalysisDashboardV2 from './components/wageAnalysis-v2/analysis-dashboard.component';
// import LaborAnalysis from "./components/labor-analysis/labor-analysis.component";
// import MapChart from "./components/charts/mapchart/MapChart.componenet";
// import MsaRanking from "./components/msa-ranking/MsaRanking.component";
// import LaborSupply from "./components/labor-supply/LaborSupply.component";
// import LaborSustainability from "./components/labor-sustainability/LaborSustainability.component";
// import PropertyView from "./components/property-view/PropertyView.component";

const loading = () => (
  <div className="animated fadeIn pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
);

const Login = React.lazy(() => import("./pages/login/login.component"));
const DefaultLayout = React.lazy(() => import("./container/defaultLayout"));

function App() {
  return (
    <>
      <Suspense fallback={loading()}>
        <Switch>
          <Route exact path="/login" component={Login}></Route>
          <Route path="/" name="Home" component={DefaultLayout} />
          {/* <Route path='/cowboys' name='Home' component={CowBoys} /> */}
        </Switch>
      </Suspense>
    </>
  );
}

export default App;
