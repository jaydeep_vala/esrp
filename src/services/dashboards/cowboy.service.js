import { axiosPost, axiosGet } from "../../axios/config";

const prefix = "/api/v1/cowboy/";

export const mapChart = async (data) => {
    const response = await axiosPost(prefix + "map", data);
    return response;
};

export const dropdwonData = async (data) => {
    const response = await axiosPost(prefix + "occupation", data);
    return response;
};

export const marketOverview = async (data) => {
    const response = await axiosPost(prefix+"market-overview", data);
    return response;
};

export const marketAgeDistribution = async (data) => {
    const response = await axiosPost(prefix+"market-age-distribution", data);
    return response;
};

export const marketEducation = async (data) => {
    const response = await axiosPost(prefix+"market-educational-attainment", data);
    return response;
};


export const marketOccupationWages = async (data) => {
    const response = await axiosPost(prefix+"living-wage-calculator", data);
    return response;
};


export const marketOccupationOverview = async (data) => {
    const response = await axiosPost(prefix+"occupation-overview", data);
    return response;
};


export const marketOccupationGrowth = async (data) => {
    const response = await axiosPost(prefix+"occupation-growth", data);
    return response;
};


export const marketRaceDistribution = async (data) => { 
    const response = await axiosPost(prefix+"market-race-distribution", data);
    return response;
};




