import { axiosGet, axiosPost } from '../../axios/config';
const prefix = '/api/v1/livingwage/';

export const livingMSA = async (data) => {
  const response = await axiosGet(prefix + 'msa', data);
  return response;
};

export const livingHouseHold = async (data) => {
  const response = await axiosGet(prefix + 'house-hold', data);
  return response;
};

export const mapChart = async (data) => {
  const response = await axiosGet(prefix + 'map', data);
  return response;
};

export const getMarketOverview = async (data) => {
  const response = await axiosPost(prefix + 'market-overview', data);
  return response;
};

export const annualIncome = async (data) => {
  const response = await axiosPost(prefix + 'annual-income', data);
  return response;
};

export const getMonthlyExpanses = async (data) => {
  const response = await axiosPost(prefix + 'monthly-expenses', data);
  return response;
};

export const livingWageCalculator = async (data) => {
  const response = await axiosPost(prefix + 'calculator', data);
  return response;
};
