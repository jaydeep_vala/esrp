import { axiosGet } from "./../axios/config";

export const getLocationCoordinates = () => {
  return axiosGet("/api/v1/labor/map");
};
