import { axiosGet, axiosPost } from "./../axios/config";

export const login = (data) => {
  return axiosPost("/api/v1/login", data);
};

export const getUser = () => {
  return axiosGet("/api/v1//user");
};
