import React, { Component, Suspense } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import { selectToken } from "../redux/token/token.selector";
import { getUser } from "../services/authentication.service";
import { selectCurrentUser } from "../redux/user/user.selector";
import { setCurrentUser } from "../redux/user/user.action";

import Header from "../components/header/header.component";
import ThemeChange from "../components/theme-change/ThemeChange-component";
import Footer from "../components/footer/footer.component";

const Dashboard = React.lazy(() =>
  import("./../pages/dashboard/dashboard.component")
);
const LaborAnalytis = React.lazy(() =>
  import("./../pages/labor-analysis/labor-analysis.component")
);

const AnalysisDashboard = React.lazy(() =>
  import("./../components/wageAnalysis/analysis-dashboard.component")
);

const Cowboys = React.lazy(() =>
  import("./../components/cowboys/cowboys.component")
);
class DefaultLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  async componentDidMount() {
    if (this.props.token === null) {
      return this.props.history.push("/login");
    }
    if (this.props.token !== null && this.props.user === null) {
      const res = await getUser();
      if (res.status !== "error") {
        if (res.data && res.data !== null && res.data !== "") {
          this.props.setcurrentuser(res.data);
        } else {
          return this.props.history.push("/login");
        }
      }
    }
    // if (this.props.user && this.props.user.role !== "admin") {
    //   const index = this.state.navigation.items.findIndex(
    //     (e) => e.name === "Settings"
    //   );
    //   if (index > -1) this.state.navigation.items.splice(index, 1);
    //   this.setState({ navigation: { ...this.state.navigation } });
    // }
  }

  loading = () => (
    <div className="animated fadeIn pt-1 text-center">
      <div className="sk-spinner sk-spinner-pulse"></div>
    </div>
  );

  render() {
    return (
      <div className="App">
        <ThemeChange />
        {/* <Sidebar /> */}
        {/* has-sidebar */}
        <div className="site-content" id="SiteContent">
          <Header />
          <div className="site-inner-content">
            <Suspense fallback={this.loading()}>
              <Switch>
                {/* <Route exact path='/' component={Dashboard}></Route> */}
                <Redirect from="/" exact={true} to="/cowboys" />
                <Route
                  exact
                  path="/labor-analysis"
                  name="labor-analysis"
                  component={LaborAnalytis}
                ></Route>
                <Route
                  exact
                  path="/cowboys"
                  name="cowboys"
                  component={Cowboys}
                ></Route>
                <Route
                  exact
                  path="/living-wage-analysis"
                  name="living-wage-analysis"
                  component={AnalysisDashboard}
                />
                {/* <Route exact path="/line-chart" component={Linechart}></Route>
            <Route exact path="/bar-chart" component={Barchart}></Route>
            <Route exact path="/informer" component={Informer}></Route>
            <Route exact path="/chart-table" component={ChartTable}></Route>
            <Route exact path="/loading" component={Loading}></Route>
            <Route exact path="/donut" component={DonutChart}></Route>
            <Route exact path="/button" component={ButtonCompo}></Route>
            <Route exact path="/circle" component={CircleChart}></Route>
            <Route exact path="/bubble" component={BubbleChart}></Route>
            <Route exact path="/column" component={ColumnChart}></Route>
            <Route exact path="/timeline" component={TimelineChart}></Route>
            <Route exact path="/dot" component={DotChart}></Route>
            <Route exact path="/map-chart" component={MapChart}></Route>
            <Route
              exact
              path="/analysis-dashboard-v2"
              component={AnalysisDashboardV2}
            ></Route>
            <Route exact path="/dashboard" component={Dashboard}></Route>
            <Route
              exact
              path="/analysis-dashboard"
              component={AnalysisDashboard}
            ></Route>
            <Route
              exact
              path="/labor-analysis"
              component={LaborAnalysis}
            ></Route>
            <Route exact path="/cowboys" component={CowBoys}></Route>

            <Route exact path="/property-view" component={PropertyView}></Route>
            <Route exact path="/msa-ranking" component={MsaRanking}></Route>
            <Route exact path="/labor-supply" component={LaborSupply}></Route>
            <Route
              exact
              path="/labor-sustainability"
              component={LaborSustainability}
            ></Route> */}
                {this.props.token !== null ? (
                  <Redirect from="/" exact={true} to="/dashboard" />
                ) : null}
              </Switch>
            </Suspense>
          </div>
          <Footer />

          <div className="fiexd-name">
            <span className="small" data-letter="JV"></span>
            <span className="bic">Jaydeep Vala</span>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  token: selectToken,
  user: selectCurrentUser,
});

const mapDispatchToProps = (dispatch) => ({
  setcurrentuser: (user) => dispatch(setCurrentUser(user)),
});

export default connect(mapStateToProps, mapDispatchToProps)(DefaultLayout);
