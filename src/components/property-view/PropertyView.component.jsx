import React, { useRef, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { selectThemeSetting } from "../../redux/theme/theme.selector";
import { setThemeSetting } from "../../redux/theme/theme.action";
import mapboxgl from '!mapbox-gl'; // eslint-disable-line import/no-webpack-loader-syntax
import Chart from "react-apexcharts";
import './PropertyView.style.scss';


const geojson = {
  'type': 'FeatureCollection',
  'features': [
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Point',
        'coordinates': [-96.7970, 32.7767]
      },
      'properties': {
        'title': '1',
        'description': ''
      }
    },
  ]
};

const geojson2 = {
  'type': 'FeatureCollection',
  'features': [
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Point',
        'coordinates': [-96.7970, 32.7767]
      },
      'properties': {
        'title': '1',
        'description': ''
      }
    },
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Point',
        'coordinates': [-96.798206, 32.778479]
      },
      'properties': {
        'title': '1',
        'description': ''
      }
    },
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Point',
        'coordinates': [-96.795608, 32.779345]
      },
      'properties': {
        'title': '1',
        'description': ''
      }
    },
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Point',
        'coordinates': [-96.793632, 32.778497]
      },
      'properties': {
        'title': '1',
        'description': ''
      }
    },
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Point',
        'coordinates': [-96.792696, 32.776862]
      },
      'properties': {
        'title': '1',
        'description': ''
      }
    },
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Point',
        'coordinates': [-96.795959, 32.774770]
      },
      'properties': {
        'title': '1',
        'description': ''
      }
    },
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Point',
        'coordinates': [-96.803646, 32.774950]
      },
      'properties': {
        'title': '1',
        'description': ''
      }
    },
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Point',
        'coordinates': [-96.799137, 32.774986]
      },
      'properties': {
        'title': '1',
        'description': ''
      }
    },
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Point',
        'coordinates': [-96.807370, 32.776097]
      },
      'properties': {
        'title': '1',
        'description': ''
      }
    },
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Point',
        'coordinates': [-96.809281, 32.779561]
      },
      'properties': {
        'title': '1',
        'description': ''
      }
    },
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Point',
        'coordinates': [-96.806403, 32.782610]
      },
      'properties': {
        'title': '1',
        'description': ''
      }
    },
  ]
};

const geojson3 = {
  'type': 'FeatureCollection',
  'features': [
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Point',
        'coordinates': [-96.7970, 32.7767]
      },
      'properties': {
        'title': '1',
        'description': ''
      }
    },
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Point',
        'coordinates': [-96.798206, 32.778479]
      },
      'properties': {
        'title': '1',
        'description': ''
      }
    },
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Point',
        'coordinates': [-96.795608, 32.779345]
      },
      'properties': {
        'title': '1',
        'description': ''
      }
    },
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Point',
        'coordinates': [-96.793632, 32.778497]
      },
      'properties': {
        'title': '1',
        'description': ''
      }
    },
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Point',
        'coordinates': [-96.792696, 32.776862]
      },
      'properties': {
        'title': '1',
        'description': ''
      }
    },
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Point',
        'coordinates': [-96.795959, 32.774770]
      },
      'properties': {
        'title': '1',
        'description': ''
      }
    },
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Point',
        'coordinates': [-96.803646, 32.774950]
      },
      'properties': {
        'title': '1',
        'description': ''
      }
    },
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Point',
        'coordinates': [-96.799137, 32.774986]
      },
      'properties': {
        'title': '1',
        'description': ''
      }
    },
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Point',
        'coordinates': [-96.807370, 32.776097]
      },
      'properties': {
        'title': '1',
        'description': ''
      }
    },
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Point',
        'coordinates': [-96.809281, 32.779561]
      },
      'properties': {
        'title': '1',
        'description': ''
      }
    },
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Point',
        'coordinates': [-96.806403, 32.782610]
      },
      'properties': {
        'title': '1',
        'description': ''
      }
    },

  ]
};
const PropertyView = ({ theme, setThemeSetting }) => {

  const mapContainer = useRef(null);
  const map = useRef(null);
  const mapContainer2 = useRef(null);
  const map2 = useRef(null);
  const mapContainer3 = useRef(null);
  const map3 = useRef(null);
  let mapColor;
  const [lng, setLng] = useState(-96.7970);
  const [lat, setLat] = useState(32.7767);
  const [zoom, setZoom] = useState(15);

  useEffect(() => {
    document.body.classList.remove('light-theme', 'dark-theme');
    document.body.classList.add(theme.theme_color);

    if (theme.theme_color === 'esrp-theme') {
      mapColor = 'mapbox://styles/basalsmartsolutions/ckvtyjs1z2hcx14oyb5axlvlc'
      if (map.current) map.current.setStyle(mapColor);
    }
    if (theme.theme_color === 'light-theme') {
      mapColor = 'mapbox://styles/basalsmartsolutions/ckw232our0h7q14qs4h6yv2bx'
      if (map.current) map.current.setStyle(mapColor);
    }
    if (theme.theme_color === 'dark-theme') {
      mapColor = 'mapbox://styles/basalsmartsolutions/ckw219z4h1r7s14qtbw4fz3x8';
      if (map.current) map.current.setStyle(mapColor);
    }

    if (theme.theme_color === 'esrp-theme') {
      mapColor = 'mapbox://styles/basalsmartsolutions/ckvtyjs1z2hcx14oyb5axlvlc'
      if (map2.current) map2.current.setStyle(mapColor);
    }
    if (theme.theme_color === 'light-theme') {
      mapColor = 'mapbox://styles/basalsmartsolutions/ckw232our0h7q14qs4h6yv2bx'
      if (map2.current) map2.current.setStyle(mapColor);
    }
    if (theme.theme_color === 'dark-theme') {
      mapColor = 'mapbox://styles/basalsmartsolutions/ckw219z4h1r7s14qtbw4fz3x8';
      if (map2.current) map2.current.setStyle(mapColor);
    }

    if (theme.theme_color === 'esrp-theme') {
      mapColor = 'mapbox://styles/basalsmartsolutions/ckvtyjs1z2hcx14oyb5axlvlc'
      if (map3.current) map3.current.setStyle(mapColor);
    }
    if (theme.theme_color === 'light-theme') {
      mapColor = 'mapbox://styles/basalsmartsolutions/ckw232our0h7q14qs4h6yv2bx'
      if (map3.current) map3.current.setStyle(mapColor);
    }
    if (theme.theme_color === 'dark-theme') {
      mapColor = 'mapbox://styles/basalsmartsolutions/ckw219z4h1r7s14qtbw4fz3x8';
      if (map3.current) map3.current.setStyle(mapColor);
    }

    if (map.current) return;
    if (mapColor) {
      map.current = new mapboxgl.Map({
        container: mapContainer.current,
        style: mapColor,
        center: [lng, lat],
        zoom: zoom
      });

      for (const feature of geojson.features) {
        // create a HTML element for each feature
        const el = document.createElement('div');
        el.className = 'marker';

        // make a marker for each feature and add it to the map
        new mapboxgl.Marker(el)
          .setLngLat(feature.geometry.coordinates)
          .setPopup(
            new mapboxgl.Popup({ offset: 25 }) // add popups
              .setHTML(
                `<span class="title">${feature.properties.title}</span><span class="sub-title">${feature.properties.description}</span>`
              )
          )
          .addTo(map.current);
      }

    }

    if (map2.current) return;
    if (mapColor) {
      map2.current = new mapboxgl.Map({
        container: mapContainer2.current,
        style: mapColor,
        center: [lng, lat],
        zoom: zoom
      });

      for (const feature of geojson2.features) {
        // create a HTML element for each feature
        const el = document.createElement('div');
        el.className = 'marker';

        // make a marker for each feature and add it to the map
        new mapboxgl.Marker(el)
          .setLngLat(feature.geometry.coordinates)
          .setPopup(
            new mapboxgl.Popup({ offset: 25 }) // add popups
              .setHTML(
                `<span class="title">${feature.properties.title}</span><span class="sub-title">${feature.properties.description}</span>`
              )
          )
          .addTo(map2.current);
      }

    }

    if (map3.current) return;
    if (mapColor) {
      map3.current = new mapboxgl.Map({
        container: mapContainer3.current,
        style: mapColor,
        center: [lng, lat],
        zoom: zoom
      });

      for (const feature of geojson3.features) {
        // create a HTML element for each feature
        const el = document.createElement('div');
        el.className = 'marker';

        // make a marker for each feature and add it to the map
        new mapboxgl.Marker(el)
          .setLngLat(feature.geometry.coordinates)
          .setPopup(
            new mapboxgl.Popup({ offset: 25 }) // add popups
              .setHTML(
                `<span class="title">${feature.properties.title}</span><span class="sub-title">${feature.properties.description}</span>`
              )
          )
          .addTo(map3.current);
      }

    }

  }, [theme.theme_color]);


  // FOR VISIT TIME CHART
  const visitTimeChart = {
    series: [
      {
        name: 'One',
        data: [667, 1144, 875, 1173, 1097, 694, 657, null, null, null, null, null, null, null],
      },
      {
        name: 'Two',
        data: [null, null, null, null, null, null, 475, 317, 736, 705, 212, 689, 764, 355, 527],
      },
    ],
    options: {
      plotOptions: {
        pie: {
          expandOnClick: false
        }
      },
      chart: {
        type: 'line',
        dropShadow: {
          enabled: false,
          color: '#000',
          top: 18,
          left: 7,
          blur: 10,
          opacity: 0,
        },
        toolbar: {
          show: false,
          tools: {
            download: false,
            selection: true,
            zoomin: '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M11 11V5H13V11H19V13H13V19H11V13H5V11H11Z" fill="#5A5A89"></path></svg>',
            zoomout: '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M11 11H13H19V13H13H11H5V11H11Z" fill="#5A5A89"></path></svg>',
            zoom: false,
            pan: '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M16 9V6L20 10L16 14V11H11V16H14L10 20L6 16H9V11H4V14L0 10L4 6V9H9V4H6L10 0L14 4H11V9H16Z" fill="#5A5A89"></path></svg>',
            reset: false,
          },
        }
      },
      colors: ['#5C86C1', '#81CAB2'],
      dataLabels: {
        enabled: true,
        // formatter: function (value) {
        //   var dataPoint = value;
        //   return dataPoint >= 1000 ? '$' + dataPoint.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : '$' + dataPoint
        // }
      },
      stroke: {
        curve: 'smooth',
      },
      grid: {
        borderColor: 'transparent',
        column: {
          colors: ['#F4F5FB', 'transparent'], // takes an array which will be repeated on columns
          opacity: 0.5,
        },
      },
      markers: {
        size: 1,
      },
      xaxis: {
        categories: ['Dec 2018', 'Apr 2019', 'Aug 2019', 'Dec 2019', 'Apr 2020', 'Aug 2020', 'Dec 2020', 'Apr 2021', 'Apr 2021', 'Apr 2021', 'Apr 2021', 'Dec 2018', 'Apr 2019', 'Aug 2019', 'Aug 2019'],
        axisTicks: {
          show: false,
          borderType: 'solid',
          color: '#78909C',
          height: 6,
        },
      },
      yaxis: {
        // min: 5,
        // max: 40,
      },
      legend: {
        position: 'top',
        horizontalAlign: 'right',
        floating: true,
        offsetY: -25,
        offsetX: -5,
      },
    },
  };

  // FOR BUSY CHART
  const busyChart = {
    series: [{
      name: 'One',
      data: [4.21, 17.79, 16.86, 18.48, 17.95, 17.33, 7.37]
    }],
    options: {
      chart: {
        type: 'bar',
        height: 350,
        toolbar: false,
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: '8%',
          borderRadius: 11,
          dataLabels: {
            position: 'top',
          },
        },
      },
      dataLabels: {
        enabled: true,
        formatter: function (val) {
          return val + "%";
        },
        offsetY: -20,
      },
      fill: {
        colors: ['#5C86C1'],
        opacity: 1,
      },

      xaxis: {
        categories: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
        axisBorder: {
          show: false,
        },
        axisTicks: {
          show: false,
        }
      },
      yaxis: {
        axisBorder: {
          show: false,
        }
      },
      grid: {
        xaxis: {
          lines: {
            show: false
          }
        },
        yaxis: {
          lines: {
            show: false
          }
        },
        row: {
          opacity: 0.5
        },
        column: {
          opacity: 0.5
        },
      },
    },
  };

  // FOR BUSY BY TIME CHART
  const busyTimeChart = {
    series: [{
      name: 'one',
      data: [0, 0, 0, 0, 0.11, 0.19, 0.27, 0.99, 2.74, 7.66, 11.57, 12.95, 10.04, 11.44, 13.62, 13.07, 8.9, 3.90, 1.69, 0.33, 0, 0, 0, 0]
    }],
    options: {
      chart: {
        type: 'bar',
        height: 350,
        toolbar: false,
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: '28%',
          borderRadius: 11,
          dataLabels: {
            position: 'top',
          },
        },
      },
      dataLabels: {
        enabled: true,
        formatter: function (val) {
          return val + "%";
        },
        offsetY: -20,
      },
      fill: {
        colors: ['#5C86C1'],
        opacity: 1,
      },

      xaxis: {
        categories: ['12a', '1a', '2a', '3a', '4a', '5a', '6a', '7a', '8a', '9a', '10a', '11a', '1p', '2p', '3p', '4p', '5p', '6p', '7p', '8p', '9p', '10p', '11p', '12p'],
        axisBorder: {
          show: false,
        },
        position: 'bottom',
        axisTicks: {
          show: false,
        }
      },
      yaxis: {
        axisBorder: {
          show: false,
        }
      },
      grid: {
        xaxis: {
          lines: {
            show: false
          }
        },
        yaxis: {
          lines: {
            show: false
          }
        },
        row: {
          opacity: 0.5
        },
        column: {
          opacity: 0.5
        },
      },
    },
  };

  // FOR BAR CHART
  const visitorChart = {
    series: [{
      data: [
        333060,
        338353,
        349297,
        326476,
        315397,
        376591,
        365531,
        357836,
        335924,
        337267,
        306401,
        291329,
        238376,
        182777,
      ]
    }],
    options: {
      chart: {
        type: 'bar',
        toolbar: {
          show: false,
        },
        tooltip: {
          show: true,
        }
      },
      grid: {
        borderColor: 'transparent',
      },
      plotOptions: {
        bar: {
          startingShape: 'rounded',
          borderRadius: 16,
          barHeight: '80%',
          distributed: true,
          horizontal: true,
          dataLabels: {
            position: 'bottom'
          },
        }
      },
      colors: [
        '#5C86C1',
        '#5C86C1',
        '#5C86C1',
        '#5C86C1',
        '#5C86C1',
        '#5C86C1',
        '#5C86C1',
        '#5C86C1',
        '#5C86C1',
        '#3FB7F3',
        '#3FB7F3',
        '#3FB7F3',
        '#3FB7F3',
        '#3FB7F3',
        '#81CAB2',
        '#81CAB2',
        '#81CAB2',
        '#81CAB2',
      ],
      dataLabels: {
        enabled: true,
        textAnchor: 'start',
        style: {
          colors: ['#fff']
        },
        formatter: function (val, opt) {
          var amount = parseInt(val);
          if (amount > 1000 && amount < 100000) {
            amount = amount / 1000;
            amount = Math.round(amount).toString();
            amount = amount + 'k';
          }
          return amount
        },
        offsetX: 0,
        dropShadow: {
          enabled: true
        }
      },
      stroke: {
        width: 1,
        colors: ['transparent']
      },
      legend: {
        show: false,
      },
      xaxis: {
        categories: [
          'Across the Ages hello hello',
          'A01 American Royalty',
          'F22 Fast Track Coupl lorem ipsum',
          '054 Influenced by In lorem ipsum',
          'C13 Philanthropic So lorem ipsum',
          'K37 Wired for Success',
          'C11 Sophistocated Ci lorem ipsum',
          'A05 Couples with Cl lorem ipsum',
          'A03 Kids and Cabern lorem ipsum',
          'Q65 Mature and Wise',
          'D18 Suburban Nightl lorem ipsum',
          'H26 Progressive Ass lorem ipsum',
          'B10 Cosmopolitian A lorem ipsum',
          'D16 Settled in Subur lorem ipsum',
        ],
        labels: {
          show: false
        },
        axisBorder: {
          show: false,
        },
        axisTicks: {
          show: false,
        }
      },
      yaxis: {
        categories: [
          'Across the Ages hello hello',
          'A01 American Royalty',
          'F22 Fast Track Coupl lorem ipsum',
          '054 Influenced by In lorem ipsum',
          'C13 Philanthropic So lorem ipsum',
          'K37 Wired for Success',
          'C11 Sophistocated Ci lorem ipsum',
          'A05 Couples with Cl lorem ipsum',
          'A03 Kids and Cabern lorem ipsum',
          'Q65 Mature and Wise',
          'D18 Suburban Nightl lorem ipsum',
          'H26 Progressive Ass lorem ipsum',
          'B10 Cosmopolitian A lorem ipsum',
          'D16 Settled in Subur lorem ipsum',
        ],
        // labels: {
        //   show: true
        // },
        labels: {
          show: true,
          rotate: 0,
          rotateAlways: false,
          hideOverlappingLabels: true,
          align: 'left',
          maxWidth: 150,
          offsetX: 143,
          trim: true,
        },
        opposite: false,
      },
      tooltip: {
        // theme: 'dark',
        custom: function ({ series, seriesIndex, dataPointIndex, w }) {
          var dataPoint = series[seriesIndex][dataPointIndex];
          return dataPoint >= 1000 ? '$' + dataPoint.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : '$' + dataPoint
        }
      }
    },
  };


  // FOR BUSY BY TIME CHART
  const merchandiseTrade = {
    series: [{
      name: 'one',
      data: [
        500,
        450,
        350,
        325,
        300,
        200,
        195,
        190,
        185,
        180,
        175,
        170,
        165,
        160,
        155,
        150,
        145,
        140,
        135,
        130,
        125,
        120,
        115,
        110,
        105,
        100,
        95,
        90,
        85,
        80,
        75,
        70,
        65,
        60,
        55,
        50,
        45,
        40,
        35,
        30,
        25,
        20,
        15,
        10,
        5,
        4,
        4,
      ]
    }],
    options: {
      chart: {
        type: 'bar',
        height: 350,
        toolbar: false,
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: '60%',
          borderRadius: 13,

        },
      },
      dataLabels: {
        enabled: false,
        formatter: function (val) {
          return val + "%";
        },
        offsetY: -20,
      },
      fill: {
        colors: ['#5C86C1'],
        opacity: 1,
      },

      xaxis: {
        categories: [
          'Cars, trucks, motorcycles',
          'Groceries & other foods f...',
          'Drugs, health aids, beau...',
          'Meals, snacks, & beverag...',
          'Automotive fuels',
          'All other merchandise',
          'Women’s, juniors, and m...',
          'Dimensional lumber & o...',
          'Furniture, sleep equipm...',
          'Packaged liquor, wine, b...',
          'Computer hardware, sof...',
          'Hardware, tools, & plum...',
          'Lawn, garden & farm eq...',
          'Men’s wear, including a...',
          'Footwear, including acc...',
          'Cigars, etc & smoker’s ac...',
          'Alchoholic beverages ser..',
          'Kitchenware & home fur...',
          'Household fuels, includ...',
          'Line meals, snacks & bvgs',
          'Jewelry',
          'Pets, pet foods, & pet su..',
          'Sporting goods & recrea...',
          'TVs, video recorders, vid...',
          'Major household applian...',
          'Soaps, detergents, & hou...',
          'Toys, hobby goods & ga...',
          'Curtains, draperies, blin...',
          'Childrens wear',
          'Paper & related prod, inc...',
          'Audio equip, musical ins...',
          'Floording and floor cove...',
          'Paints & sundries',
          'Line meals, snacks & no...',
          'Boats & other sports veh...',
          'Optical Goods',
          'Small electric appliances',
          'Books',
          'RVs, incl camping & travel',
          'Automobile lubricants',
          'Photographic equipment',
          'Other services',
          'Sewing, knitting materia...',
          'Baby goods: incl bottles...',
        ],
        axisBorder: {
          show: false,
        },
        position: 'bottom',
        axisTicks: {
          show: false,
        },
        labels: {
          show: true,
          rotate: -45,
          rotateAlways: false,
          hideOverlappingLabels: true,
          showDuplicates: false,
          trim: false,
          minHeight: undefined,
          maxHeight: 120,
          offsetX: 0,
          offsetY: 0,
          format: undefined,
          formatter: undefined,
          datetimeUTC: true,
          datetimeFormatter: {
            year: 'yyyy',
            month: "MMM 'yy",
            day: 'dd MMM',
            hour: 'HH:mm',
          },
        },
      },
      yaxis: {
        axisBorder: {
          show: false,
        }
      },
      grid: {
        xaxis: {
          lines: {
            show: false
          }
        },
        yaxis: {
          lines: {
            show: false
          }
        },
        row: {
          opacity: 0.5
        },
        column: {
          opacity: 0.5
        },
      },
    },
  };

  return (
    <>
      <section className="property-view-sec">
        <div className="container">
          <div className="row title-filter-row">
            <div className="col col-6">
              <div className="breadcrumbs">
                <ul>
                  <li className="breadcrumb-item">
                    <a href="#!">Home</a>
                  </li>
                  <li className="breadcrumb-item">Dashboards</li>
                </ul>
              </div>
              <h3 className="page-title">Property View</h3>

            </div>
            <div className="col col-6">
              <div className="analysis-filter-wrapper">
                <div className="analysis-filter-item">
                  <button className='btn rounded outline'>Download</button>
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col col-12">
              <div className="card property-view-card">
                <div className="card-body">
                  <div className="card-title">
                    <label htmlFor="">Property View</label>
                    <span className="big-text">Amegy Bank</span>
                    <span className="sub-text">2601 Dallas Pkwy, Plano, TX</span>
                  </div>
                  <div className="card-content">

                    <div className="chart-palette-wrapper">
                      <div className="row">
                        <div className="col col-4">
                          <div className="card chart-palette-item green">
                            <div className="card-body">
                              {/* <label className="title">The average salary for Lawyer is</label> */}
                              <h3>4K</h3>
                              <span>Estimated number of Customers</span>
                            </div>
                          </div>
                        </div>
                        <div className="col col-4">
                          <div className="card chart-palette-item blue">
                            <div className="card-body">
                              {/* <label className="title">The average salary for Lawyer is</label> */}
                              <h3>4K</h3>
                              <span>Estimated number of Customers</span>
                            </div>
                          </div>
                        </div>
                        <div className="col col-4">
                          <div className="card chart-palette-item purple">
                            <div className="card-body">
                              {/* <label className="title">The average salary for Lawyer is</label> */}
                              <h3>4K</h3>
                              <span>Estimated number of Customers</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="map-line-chart-wrapper">
                      <div className="row">
                        <div className="col col-4">
                          <div className="property-map-box property-box-wrapper">
                            <div className="title">
                              <h3 className=''>Map View</h3>
                            </div>
                            <div className="map-wrapper multi-circle-chart-map-wrap">
                              {map && <div ref={mapContainer} className="map-container dashboard-map" />}
                            </div>
                          </div>
                        </div>
                        <div className="col col-8">
                          <div className="property-line-chart-box property-box-wrapper">
                            <div className="title">
                              <h3 className=''>Visits Over Time</h3>
                              <div className="more-btn">
                                <a>
                                  <svg width="4" height="20" viewBox="0 0 4 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M0.0658571 2.35131C0.0658571 3.36445 0.799037 4.18577 1.70346 4.18577C2.60788 4.18577 3.34106 3.36445 3.34106 2.35131C3.34106 1.33816 2.60788 0.516846 1.70346 0.516846C0.799037 0.516846 0.0658571 1.33816 0.0658571 2.35131ZM0.0658568 9.68916C0.0658568 10.7023 0.799037 11.5236 1.70346 11.5236C2.60788 11.5236 3.34106 10.7023 3.34106 9.68917C3.34106 8.67602 2.60788 7.8547 1.70346 7.8547C0.799037 7.8547 0.0658569 8.67602 0.0658568 9.68916Z" fill="#7C7C84" />
                                    <ellipse cx="1.70346" cy="17.3581" rx="1.83446" ry="1.6376" transform="rotate(90 1.70346 17.3581)" fill="#7C7C84" />
                                    <ellipse cx="1.70346" cy="9.6892" rx="1.83446" ry="1.6376" transform="rotate(90 1.70346 9.6892)" fill="#7C7C84" />
                                  </svg>
                                </a>
                              </div>
                            </div>
                            <div className="chart-wrapper growth-lawyers-card">
                              <Chart
                                options={visitTimeChart.options}
                                series={visitTimeChart.series}
                                type='line'
                                width='100%'
                                height="100%"
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="average-visitors-palette-wrapper">
                      <div className="row">
                        <div className="col col-12  property-box-wrapper">
                          <div className="title">
                            <h3 className=''>Average Daily Visitors</h3>
                            <div className="more-btn">
                              <a>
                                <svg width="4" height="20" viewBox="0 0 4 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                  <path fill-rule="evenodd" clip-rule="evenodd" d="M0.0658571 2.35131C0.0658571 3.36445 0.799037 4.18577 1.70346 4.18577C2.60788 4.18577 3.34106 3.36445 3.34106 2.35131C3.34106 1.33816 2.60788 0.516846 1.70346 0.516846C0.799037 0.516846 0.0658571 1.33816 0.0658571 2.35131ZM0.0658568 9.68916C0.0658568 10.7023 0.799037 11.5236 1.70346 11.5236C2.60788 11.5236 3.34106 10.7023 3.34106 9.68917C3.34106 8.67602 2.60788 7.8547 1.70346 7.8547C0.799037 7.8547 0.0658569 8.67602 0.0658568 9.68916Z" fill="#7C7C84" />
                                  <ellipse cx="1.70346" cy="17.3581" rx="1.83446" ry="1.6376" transform="rotate(90 1.70346 17.3581)" fill="#7C7C84" />
                                  <ellipse cx="1.70346" cy="9.6892" rx="1.83446" ry="1.6376" transform="rotate(90 1.70346 9.6892)" fill="#7C7C84" />
                                </svg>
                              </a>
                            </div>
                          </div>
                        </div>
                        <div className="col col-6">
                          <div className="card visitors-palette-card blue">
                            <div className="card-body">
                              <h3>202.75</h3>
                              <span>Before March 1 2020</span>
                            </div>
                          </div>
                        </div>
                        <div className="col col-6">
                          <div className="card visitors-palette-card green">
                            <div className="card-body">
                              <h3>112.64</h3>
                              <span>After March 1 2020</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="row busy-chart-row">
            <div className="col col-12">
              <div className="card busy-chart-card">
                <div className="card-body">
                  <div className="card-title">
                    <label for="">Busiest Days by Visit</label>
                  </div>
                  <div className="card-content">
                    <div className="busy-chart-wrapper">
                      <Chart
                        options={busyChart.options}
                        series={busyChart.series}
                        type="bar"
                        width="100%"
                        height="100%"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="row busy-time-chart-row">
            <div className="col col-12">
              <div className="card busy-time-chart-card">
                <div className="card-body">
                  <div className="card-title">
                    <label for="">Busiest Days by Visit</label>
                  </div>
                  <div className="card-content">
                    <div className="busy-time-chart-wrapper">
                      <Chart
                        options={busyTimeChart.options}
                        series={busyTimeChart.series}
                        type="bar"
                        width="100%"
                        height="100%"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="row visitor-live-row">
            <div className="col col-8">
              <div className="card labor-situational-map-card  visitor-live-card">
                <div className="card-body">
                  <div className="card-title">
                    <label>Where your Visitors Live</label>
                    <div className="more-btn">
                      <a>
                        <svg width="4" height="20" viewBox="0 0 4 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path fill-rule="evenodd" clip-rule="evenodd" d="M0.0658571 2.35131C0.0658571 3.36445 0.799037 4.18577 1.70346 4.18577C2.60788 4.18577 3.34106 3.36445 3.34106 2.35131C3.34106 1.33816 2.60788 0.516846 1.70346 0.516846C0.799037 0.516846 0.0658571 1.33816 0.0658571 2.35131ZM0.0658568 9.68916C0.0658568 10.7023 0.799037 11.5236 1.70346 11.5236C2.60788 11.5236 3.34106 10.7023 3.34106 9.68917C3.34106 8.67602 2.60788 7.8547 1.70346 7.8547C0.799037 7.8547 0.0658569 8.67602 0.0658568 9.68916Z" fill="#7C7C84" />
                          <ellipse cx="1.70346" cy="17.3581" rx="1.83446" ry="1.6376" transform="rotate(90 1.70346 17.3581)" fill="#7C7C84" />
                          <ellipse cx="1.70346" cy="9.6892" rx="1.83446" ry="1.6376" transform="rotate(90 1.70346 9.6892)" fill="#7C7C84" />
                        </svg>
                      </a>
                    </div>
                  </div>

                  <div className="map-control-wrapper">
                    <div className="labor-map-controls">
                      <div className="map-control-wrapper">
                        <div className="map-control-item">
                          <ul>
                            <li>
                              <a>
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                  <path d="M11 11V5H13V11H19V13H13V19H11V13H5V11H11Z" fill="#5A5A89" />
                                </svg>
                              </a>
                            </li>
                            <li>
                              <a>
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                  <path d="M11 11H13H19V13H13H11H5V11H11Z" fill="#5A5A89" />
                                </svg>
                              </a>
                            </li>
                          </ul>
                        </div>
                        <div className="map-control-item d-none">
                          <ul>
                            <li>
                              <a>
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                  <path d="M12 20C14.1217 20 16.1566 19.1571 17.6569 17.6569C19.1571 16.1566 20 14.1217 20 12C20 9.87827 19.1571 7.84344 17.6569 6.34315C16.1566 4.84285 14.1217 4 12 4C9.87827 4 7.84344 4.84285 6.34315 6.34315C4.84285 7.84344 4 9.87827 4 12C4 14.1217 4.84285 16.1566 6.34315 17.6569C7.84344 19.1571 9.87827 20 12 20ZM12 22C6.477 22 2 17.523 2 12C2 6.477 6.477 2 12 2C17.523 2 22 6.477 22 12C22 17.523 17.523 22 12 22ZM12 14C11.4696 14 10.9609 13.7893 10.5858 13.4142C10.2107 13.0391 10 12.5304 10 12C10 11.4696 10.2107 10.9609 10.5858 10.5858C10.9609 10.2107 11.4696 10 12 10C12.5304 10 13.0391 10.2107 13.4142 10.5858C13.7893 10.9609 14 11.4696 14 12C14 12.5304 13.7893 13.0391 13.4142 13.4142C13.0391 13.7893 12.5304 14 12 14Z" fill="#5A5A89" />
                                </svg>
                              </a>
                            </li>
                          </ul>
                        </div>
                        <div className="map-control-item">
                          <ul>
                            <li><a>
                              <svg width="18" height="22" viewBox="0 0 18 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M9 18.8999L13.95 13.9499C14.9289 12.9709 15.5955 11.7236 15.8656 10.3658C16.1356 9.00795 15.9969 7.60052 15.4671 6.32148C14.9373 5.04244 14.04 3.94923 12.8889 3.18009C11.7378 2.41095 10.3844 2.00043 9 2.00043C7.61557 2.00043 6.26222 2.41095 5.11109 3.18009C3.95996 3.94923 3.06275 5.04244 2.53292 6.32148C2.00308 7.60052 1.86442 9.00795 2.13445 10.3658C2.40449 11.7236 3.07111 12.9709 4.05 13.9499L9 18.8999ZM9 21.7279L2.636 15.3639C1.37734 14.1052 0.520187 12.5016 0.172928 10.7558C-0.17433 9.00995 0.00390685 7.20035 0.685099 5.55582C1.36629 3.91129 2.51984 2.50569 3.99988 1.51677C5.47992 0.527838 7.21998 0 9 0C10.78 0 12.5201 0.527838 14.0001 1.51677C15.4802 2.50569 16.6337 3.91129 17.3149 5.55582C17.9961 7.20035 18.1743 9.00995 17.8271 10.7558C17.4798 12.5016 16.6227 14.1052 15.364 15.3639L9 21.7279ZM9 10.9999C9.53044 10.9999 10.0391 10.7892 10.4142 10.4141C10.7893 10.0391 11 9.53035 11 8.99992C11 8.46949 10.7893 7.96078 10.4142 7.58571C10.0391 7.21064 9.53044 6.99992 9 6.99992C8.46957 6.99992 7.96086 7.21064 7.58579 7.58571C7.21072 7.96078 7 8.46949 7 8.99992C7 9.53035 7.21072 10.0391 7.58579 10.4141C7.96086 10.7892 8.46957 10.9999 9 10.9999ZM9 12.9999C7.93914 12.9999 6.92172 12.5785 6.17158 11.8283C5.42143 11.0782 5 10.0608 5 8.99992C5 7.93906 5.42143 6.92164 6.17158 6.17149C6.92172 5.42135 7.93914 4.99992 9 4.99992C10.0609 4.99992 11.0783 5.42135 11.8284 6.17149C12.5786 6.92164 13 7.93906 13 8.99992C13 10.0608 12.5786 11.0782 11.8284 11.8283C11.0783 12.5785 10.0609 12.9999 9 12.9999Z" fill="#5A5A89" />
                              </svg>
                            </a></li>
                          </ul>
                        </div>
                        <div className="map-control-item">
                          <ul>
                            <li>
                              <a>
                                <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                  <path d="M16 9V6L20 10L16 14V11H11V16H14L10 20L6 16H9V11H4V14L0 10L4 6V9H9V4H6L10 0L14 4H11V9H16Z" fill="#5A5A89" />
                                </svg>
                              </a>
                            </li>
                          </ul>
                        </div>
                        <div className="map-control-item d-none">
                          <ul>
                            <li>
                              <a>
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                  <path d="M7.10495 15.2102C7.7586 15.4693 8.30051 15.9497 8.63613 16.5676C8.97175 17.1855 9.07975 17.9016 8.9413 18.5909C8.80285 19.2803 8.42673 19.8992 7.87858 20.3396C7.33043 20.7799 6.64505 21.0138 5.94204 21.0005C5.23903 20.9871 4.56303 20.7273 4.032 20.2665C3.50098 19.8056 3.14864 19.1729 3.03648 18.4787C2.92432 17.7846 3.05945 17.0731 3.4183 16.4685C3.77715 15.8638 4.33692 15.4043 4.99995 15.1702V8.83023C4.33245 8.59432 3.76985 8.13003 3.41159 7.51942C3.05332 6.90881 2.92246 6.1912 3.04213 5.49344C3.16181 4.79567 3.52431 4.16268 4.06557 3.70635C4.60683 3.25002 5.29199 2.99974 5.99995 2.99974C6.7079 2.99974 7.39306 3.25002 7.93432 3.70635C8.47558 4.16268 8.83808 4.79567 8.95776 5.49344C9.07743 6.1912 8.94657 6.90881 8.58831 7.51942C8.23004 8.13003 7.66744 8.59432 6.99995 8.83023V12.0002C7.83595 11.3722 8.87395 11.0002 9.99995 11.0002H13.9999C14.6581 11.0003 15.2979 10.7839 15.821 10.3846C16.3441 9.98528 16.7215 9.42506 16.8949 8.79023C16.2381 8.53013 15.6941 8.04666 15.3587 7.42492C15.0233 6.80318 14.9179 6.08304 15.0612 5.39128C15.2045 4.69952 15.5873 4.08049 16.1421 3.64317C16.6969 3.20585 17.3882 2.97827 18.0943 3.00051C18.8004 3.02275 19.476 3.29338 20.0022 3.76475C20.5284 4.23611 20.8714 4.87801 20.9709 5.57741C21.0704 6.27682 20.92 6.9889 20.5461 7.5883C20.1722 8.1877 19.5988 8.63598 18.9269 8.85423C18.7255 10.0149 18.1209 11.0672 17.2195 11.8257C16.3182 12.5842 15.178 13.0001 13.9999 13.0002H9.99995C9.34184 13.0002 8.70196 13.2165 8.17885 13.6158C7.65575 14.0152 7.27841 14.5754 7.10495 15.2102ZM5.99995 17.0002C5.73473 17.0002 5.48038 17.1056 5.29284 17.2931C5.1053 17.4807 4.99995 17.735 4.99995 18.0002C4.99995 18.2654 5.1053 18.5198 5.29284 18.7073C5.48038 18.8949 5.73473 19.0002 5.99995 19.0002C6.26516 19.0002 6.51952 18.8949 6.70705 18.7073C6.89459 18.5198 6.99995 18.2654 6.99995 18.0002C6.99995 17.735 6.89459 17.4807 6.70705 17.2931C6.51952 17.1056 6.26516 17.0002 5.99995 17.0002ZM5.99995 5.00023C5.73473 5.00023 5.48038 5.10558 5.29284 5.29312C5.1053 5.48066 4.99995 5.73501 4.99995 6.00023C4.99995 6.26544 5.1053 6.5198 5.29284 6.70733C5.48038 6.89487 5.73473 7.00023 5.99995 7.00023C6.26516 7.00023 6.51952 6.89487 6.70705 6.70733C6.89459 6.5198 6.99995 6.26544 6.99995 6.00023C6.99995 5.73501 6.89459 5.48066 6.70705 5.29312C6.51952 5.10558 6.26516 5.00023 5.99995 5.00023ZM17.9999 5.00023C17.7347 5.00023 17.4804 5.10558 17.2928 5.29312C17.1053 5.48066 16.9999 5.73501 16.9999 6.00023C16.9999 6.26544 17.1053 6.5198 17.2928 6.70733C17.4804 6.89487 17.7347 7.00023 17.9999 7.00023C18.2652 7.00023 18.5195 6.89487 18.7071 6.70733C18.8946 6.5198 18.9999 6.26544 18.9999 6.00023C18.9999 5.73501 18.8946 5.48066 18.7071 5.29312C18.5195 5.10558 18.2652 5.00023 17.9999 5.00023Z" fill="#5A5A89" />
                                </svg>
                              </a>
                            </li>
                            <li>
                              <a>
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                  <path d="M15.874 12.9999C15.6516 13.8581 15.1504 14.6182 14.4493 15.1608C13.7481 15.7034 12.8866 15.9978 12 15.9978C11.1134 15.9978 10.2519 15.7034 9.55074 15.1608C8.84957 14.6182 8.34844 13.8581 8.126 12.9999H3V10.9999H8.126C8.34844 10.1417 8.84957 9.38158 9.55074 8.83897C10.2519 8.29636 11.1134 8.00195 12 8.00195C12.8866 8.00195 13.7481 8.29636 14.4493 8.83897C15.1504 9.38158 15.6516 10.1417 15.874 10.9999H21V12.9999H15.874ZM12 13.9999C12.5304 13.9999 13.0391 13.7892 13.4142 13.4141C13.7893 13.039 14 12.5303 14 11.9999C14 11.4695 13.7893 10.9608 13.4142 10.5857C13.0391 10.2106 12.5304 9.9999 12 9.9999C11.4696 9.9999 10.9609 10.2106 10.5858 10.5857C10.2107 10.9608 10 11.4695 10 11.9999C10 12.5303 10.2107 13.039 10.5858 13.4141C10.9609 13.7892 11.4696 13.9999 12 13.9999Z" fill="#5A5A89" />
                                </svg>
                              </a>
                            </li>
                            <li>
                              <a>
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                  <path d="M15 5H17C17.5305 5 18.0392 5.21071 18.4142 5.58579C18.7893 5.96086 19 6.46957 19 7V15.17C19.6675 15.4059 20.2301 15.8702 20.5884 16.4808C20.9467 17.0914 21.0775 17.809 20.9578 18.5068C20.8382 19.2046 20.4757 19.8375 19.9344 20.2939C19.3931 20.7502 18.708 21.0005 18 21.0005C17.2921 21.0005 16.6069 20.7502 16.0657 20.2939C15.5244 19.8375 15.1619 19.2046 15.0422 18.5068C14.9225 17.809 15.0534 17.0914 15.4117 16.4808C15.7699 15.8702 16.3325 15.4059 17 15.17V7H15V10L10.5 6L15 2V5ZM5.00003 8.83C4.33254 8.59409 3.76994 8.1298 3.41167 7.51919C3.0534 6.90859 2.92254 6.19098 3.04222 5.49321C3.16189 4.79545 3.5244 4.16246 4.06566 3.70613C4.60692 3.2498 5.29208 2.99951 6.00003 2.99951C6.70798 2.99951 7.39314 3.2498 7.9344 3.70613C8.47566 4.16246 8.83817 4.79545 8.95784 5.49321C9.07752 6.19098 8.94666 6.90859 8.58839 7.51919C8.23012 8.1298 7.66752 8.59409 7.00003 8.83V15.17C7.66752 15.4059 8.23012 15.8702 8.58839 16.4808C8.94666 17.0914 9.07752 17.809 8.95784 18.5068C8.83817 19.2046 8.47566 19.8375 7.9344 20.2939C7.39314 20.7502 6.70798 21.0005 6.00003 21.0005C5.29208 21.0005 4.60692 20.7502 4.06566 20.2939C3.5244 19.8375 3.16189 19.2046 3.04222 18.5068C2.92254 17.809 3.0534 17.0914 3.41167 16.4808C3.76994 15.8702 4.33254 15.4059 5.00003 15.17V8.83ZM6.00003 7C6.26525 7 6.5196 6.89464 6.70714 6.70711C6.89467 6.51957 7.00003 6.26522 7.00003 6C7.00003 5.73478 6.89467 5.48043 6.70714 5.29289C6.5196 5.10536 6.26525 5 6.00003 5C5.73481 5 5.48046 5.10536 5.29292 5.29289C5.10539 5.48043 5.00003 5.73478 5.00003 6C5.00003 6.26522 5.10539 6.51957 5.29292 6.70711C5.48046 6.89464 5.73481 7 6.00003 7ZM6.00003 19C6.26525 19 6.5196 18.8946 6.70714 18.7071C6.89467 18.5196 7.00003 18.2652 7.00003 18C7.00003 17.7348 6.89467 17.4804 6.70714 17.2929C6.5196 17.1054 6.26525 17 6.00003 17C5.73481 17 5.48046 17.1054 5.29292 17.2929C5.10539 17.4804 5.00003 17.7348 5.00003 18C5.00003 18.2652 5.10539 18.5196 5.29292 18.7071C5.48046 18.8946 5.73481 19 6.00003 19ZM18 19C18.2652 19 18.5196 18.8946 18.7071 18.7071C18.8947 18.5196 19 18.2652 19 18C19 17.7348 18.8947 17.4804 18.7071 17.2929C18.5196 17.1054 18.2652 17 18 17C17.7348 17 17.4805 17.1054 17.2929 17.2929C17.1054 17.4804 17 17.7348 17 18C17 18.2652 17.1054 18.5196 17.2929 18.7071C17.4805 18.8946 17.7348 19 18 19Z" fill="#5A5A89" />
                                </svg>
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div className="multi-circle-chart-map-wrap visitor-live-map">
                      {map && <div ref={mapContainer2} className="map-container dashboard-map" />}
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="col col-4">
              <div className="card visitor-bar-chart-card">
                <div className="card-body">
                  <div className="card-title">
                    <label>Who are your Visitors?</label>
                    <span class="sub-text">Hover over chart to learn more</span>
                  </div>
                  <div className="card-content">
                    <div className="visitor-bar-chart-wrapper">
                      <Chart
                        options={visitorChart.options}
                        series={visitorChart.series}
                        type="bar"
                        width="100%"
                        height="100%"
                      />
                      <div className="visitor-bar-chart-inner">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="row trade-demographics-row">
            <div className="col col-4">
              <div class="card market-overview-card">
                <div class="card-body">
                  <div class="card-title"><label for="">True Trade Area Demographics</label></div>
                  <div className="card-content">
                    <ul>
                      <li><span class="name">Average Drive Time in Trade Area</span><span class="value">17.25 min</span></li>
                      <li><span class="name">2020 Total Population</span><span class="value">139,526</span></li>
                      <li><span class="name">2020 Total Households</span><span class="value">53,613</span></li>
                      <li><span class="name">2020 5 Year Projected Population Growth</span><span class="value">11.64%</span></li>
                      <li><span class="name">2020 Total Daytime Population</span><span class="value">162,774</span></li>
                      <li><span class="name">Median Age</span><span class="value">37</span></li>
                      <li><span class="name">Mean Household Income</span><span class="value">142,872</span></li>
                      <li><span class="name">Median Household Income</span><span class="value">103,141</span></li>
                      <li><span class="name">% Asian</span><span class="value">18.58%</span></li>
                      <li><span class="name">% Black/African American</span><span class="value">10.84%</span></li>
                      <li><span class="name">% Hispanic Latino</span><span class="value">16.52%</span></li>
                      <li><span class="name">% White</span><span class="value">60.86%</span></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div className="col col-8">
              <div className="card labor-situational-map-card  where-visitors-live-card">
                <div className="card-body">
                  <div className="card-title">
                    <label>Where your Visitors Live</label>
                    <div className="more-btn">
                      <a>
                        <svg width="4" height="20" viewBox="0 0 4 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path fill-rule="evenodd" clip-rule="evenodd" d="M0.0658571 2.35131C0.0658571 3.36445 0.799037 4.18577 1.70346 4.18577C2.60788 4.18577 3.34106 3.36445 3.34106 2.35131C3.34106 1.33816 2.60788 0.516846 1.70346 0.516846C0.799037 0.516846 0.0658571 1.33816 0.0658571 2.35131ZM0.0658568 9.68916C0.0658568 10.7023 0.799037 11.5236 1.70346 11.5236C2.60788 11.5236 3.34106 10.7023 3.34106 9.68917C3.34106 8.67602 2.60788 7.8547 1.70346 7.8547C0.799037 7.8547 0.0658569 8.67602 0.0658568 9.68916Z" fill="#7C7C84" />
                          <ellipse cx="1.70346" cy="17.3581" rx="1.83446" ry="1.6376" transform="rotate(90 1.70346 17.3581)" fill="#7C7C84" />
                          <ellipse cx="1.70346" cy="9.6892" rx="1.83446" ry="1.6376" transform="rotate(90 1.70346 9.6892)" fill="#7C7C84" />
                        </svg>
                      </a>
                    </div>
                  </div>

                  <div className="map-control-wrapper">
                    <div className="labor-map-controls">
                      <div className="map-control-wrapper">
                        <div className="map-control-item">
                          <ul>
                            <li>
                              <a>
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                  <path d="M11 11V5H13V11H19V13H13V19H11V13H5V11H11Z" fill="#5A5A89" />
                                </svg>
                              </a>
                            </li>
                            <li>
                              <a>
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                  <path d="M11 11H13H19V13H13H11H5V11H11Z" fill="#5A5A89" />
                                </svg>
                              </a>
                            </li>
                          </ul>
                        </div>
                        <div className="map-control-item d-none">
                          <ul>
                            <li>
                              <a>
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                  <path d="M12 20C14.1217 20 16.1566 19.1571 17.6569 17.6569C19.1571 16.1566 20 14.1217 20 12C20 9.87827 19.1571 7.84344 17.6569 6.34315C16.1566 4.84285 14.1217 4 12 4C9.87827 4 7.84344 4.84285 6.34315 6.34315C4.84285 7.84344 4 9.87827 4 12C4 14.1217 4.84285 16.1566 6.34315 17.6569C7.84344 19.1571 9.87827 20 12 20ZM12 22C6.477 22 2 17.523 2 12C2 6.477 6.477 2 12 2C17.523 2 22 6.477 22 12C22 17.523 17.523 22 12 22ZM12 14C11.4696 14 10.9609 13.7893 10.5858 13.4142C10.2107 13.0391 10 12.5304 10 12C10 11.4696 10.2107 10.9609 10.5858 10.5858C10.9609 10.2107 11.4696 10 12 10C12.5304 10 13.0391 10.2107 13.4142 10.5858C13.7893 10.9609 14 11.4696 14 12C14 12.5304 13.7893 13.0391 13.4142 13.4142C13.0391 13.7893 12.5304 14 12 14Z" fill="#5A5A89" />
                                </svg>
                              </a>
                            </li>
                          </ul>
                        </div>
                        <div className="map-control-item">
                          <ul>
                            <li><a>
                              <svg width="18" height="22" viewBox="0 0 18 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M9 18.8999L13.95 13.9499C14.9289 12.9709 15.5955 11.7236 15.8656 10.3658C16.1356 9.00795 15.9969 7.60052 15.4671 6.32148C14.9373 5.04244 14.04 3.94923 12.8889 3.18009C11.7378 2.41095 10.3844 2.00043 9 2.00043C7.61557 2.00043 6.26222 2.41095 5.11109 3.18009C3.95996 3.94923 3.06275 5.04244 2.53292 6.32148C2.00308 7.60052 1.86442 9.00795 2.13445 10.3658C2.40449 11.7236 3.07111 12.9709 4.05 13.9499L9 18.8999ZM9 21.7279L2.636 15.3639C1.37734 14.1052 0.520187 12.5016 0.172928 10.7558C-0.17433 9.00995 0.00390685 7.20035 0.685099 5.55582C1.36629 3.91129 2.51984 2.50569 3.99988 1.51677C5.47992 0.527838 7.21998 0 9 0C10.78 0 12.5201 0.527838 14.0001 1.51677C15.4802 2.50569 16.6337 3.91129 17.3149 5.55582C17.9961 7.20035 18.1743 9.00995 17.8271 10.7558C17.4798 12.5016 16.6227 14.1052 15.364 15.3639L9 21.7279ZM9 10.9999C9.53044 10.9999 10.0391 10.7892 10.4142 10.4141C10.7893 10.0391 11 9.53035 11 8.99992C11 8.46949 10.7893 7.96078 10.4142 7.58571C10.0391 7.21064 9.53044 6.99992 9 6.99992C8.46957 6.99992 7.96086 7.21064 7.58579 7.58571C7.21072 7.96078 7 8.46949 7 8.99992C7 9.53035 7.21072 10.0391 7.58579 10.4141C7.96086 10.7892 8.46957 10.9999 9 10.9999ZM9 12.9999C7.93914 12.9999 6.92172 12.5785 6.17158 11.8283C5.42143 11.0782 5 10.0608 5 8.99992C5 7.93906 5.42143 6.92164 6.17158 6.17149C6.92172 5.42135 7.93914 4.99992 9 4.99992C10.0609 4.99992 11.0783 5.42135 11.8284 6.17149C12.5786 6.92164 13 7.93906 13 8.99992C13 10.0608 12.5786 11.0782 11.8284 11.8283C11.0783 12.5785 10.0609 12.9999 9 12.9999Z" fill="#5A5A89" />
                              </svg>
                            </a></li>
                          </ul>
                        </div>
                        <div className="map-control-item">
                          <ul>
                            <li>
                              <a>
                                <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                  <path d="M16 9V6L20 10L16 14V11H11V16H14L10 20L6 16H9V11H4V14L0 10L4 6V9H9V4H6L10 0L14 4H11V9H16Z" fill="#5A5A89" />
                                </svg>
                              </a>
                            </li>
                          </ul>
                        </div>
                        <div className="map-control-item d-none">
                          <ul>
                            <li>
                              <a>
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                  <path d="M7.10495 15.2102C7.7586 15.4693 8.30051 15.9497 8.63613 16.5676C8.97175 17.1855 9.07975 17.9016 8.9413 18.5909C8.80285 19.2803 8.42673 19.8992 7.87858 20.3396C7.33043 20.7799 6.64505 21.0138 5.94204 21.0005C5.23903 20.9871 4.56303 20.7273 4.032 20.2665C3.50098 19.8056 3.14864 19.1729 3.03648 18.4787C2.92432 17.7846 3.05945 17.0731 3.4183 16.4685C3.77715 15.8638 4.33692 15.4043 4.99995 15.1702V8.83023C4.33245 8.59432 3.76985 8.13003 3.41159 7.51942C3.05332 6.90881 2.92246 6.1912 3.04213 5.49344C3.16181 4.79567 3.52431 4.16268 4.06557 3.70635C4.60683 3.25002 5.29199 2.99974 5.99995 2.99974C6.7079 2.99974 7.39306 3.25002 7.93432 3.70635C8.47558 4.16268 8.83808 4.79567 8.95776 5.49344C9.07743 6.1912 8.94657 6.90881 8.58831 7.51942C8.23004 8.13003 7.66744 8.59432 6.99995 8.83023V12.0002C7.83595 11.3722 8.87395 11.0002 9.99995 11.0002H13.9999C14.6581 11.0003 15.2979 10.7839 15.821 10.3846C16.3441 9.98528 16.7215 9.42506 16.8949 8.79023C16.2381 8.53013 15.6941 8.04666 15.3587 7.42492C15.0233 6.80318 14.9179 6.08304 15.0612 5.39128C15.2045 4.69952 15.5873 4.08049 16.1421 3.64317C16.6969 3.20585 17.3882 2.97827 18.0943 3.00051C18.8004 3.02275 19.476 3.29338 20.0022 3.76475C20.5284 4.23611 20.8714 4.87801 20.9709 5.57741C21.0704 6.27682 20.92 6.9889 20.5461 7.5883C20.1722 8.1877 19.5988 8.63598 18.9269 8.85423C18.7255 10.0149 18.1209 11.0672 17.2195 11.8257C16.3182 12.5842 15.178 13.0001 13.9999 13.0002H9.99995C9.34184 13.0002 8.70196 13.2165 8.17885 13.6158C7.65575 14.0152 7.27841 14.5754 7.10495 15.2102ZM5.99995 17.0002C5.73473 17.0002 5.48038 17.1056 5.29284 17.2931C5.1053 17.4807 4.99995 17.735 4.99995 18.0002C4.99995 18.2654 5.1053 18.5198 5.29284 18.7073C5.48038 18.8949 5.73473 19.0002 5.99995 19.0002C6.26516 19.0002 6.51952 18.8949 6.70705 18.7073C6.89459 18.5198 6.99995 18.2654 6.99995 18.0002C6.99995 17.735 6.89459 17.4807 6.70705 17.2931C6.51952 17.1056 6.26516 17.0002 5.99995 17.0002ZM5.99995 5.00023C5.73473 5.00023 5.48038 5.10558 5.29284 5.29312C5.1053 5.48066 4.99995 5.73501 4.99995 6.00023C4.99995 6.26544 5.1053 6.5198 5.29284 6.70733C5.48038 6.89487 5.73473 7.00023 5.99995 7.00023C6.26516 7.00023 6.51952 6.89487 6.70705 6.70733C6.89459 6.5198 6.99995 6.26544 6.99995 6.00023C6.99995 5.73501 6.89459 5.48066 6.70705 5.29312C6.51952 5.10558 6.26516 5.00023 5.99995 5.00023ZM17.9999 5.00023C17.7347 5.00023 17.4804 5.10558 17.2928 5.29312C17.1053 5.48066 16.9999 5.73501 16.9999 6.00023C16.9999 6.26544 17.1053 6.5198 17.2928 6.70733C17.4804 6.89487 17.7347 7.00023 17.9999 7.00023C18.2652 7.00023 18.5195 6.89487 18.7071 6.70733C18.8946 6.5198 18.9999 6.26544 18.9999 6.00023C18.9999 5.73501 18.8946 5.48066 18.7071 5.29312C18.5195 5.10558 18.2652 5.00023 17.9999 5.00023Z" fill="#5A5A89" />
                                </svg>
                              </a>
                            </li>
                            <li>
                              <a>
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                  <path d="M15.874 12.9999C15.6516 13.8581 15.1504 14.6182 14.4493 15.1608C13.7481 15.7034 12.8866 15.9978 12 15.9978C11.1134 15.9978 10.2519 15.7034 9.55074 15.1608C8.84957 14.6182 8.34844 13.8581 8.126 12.9999H3V10.9999H8.126C8.34844 10.1417 8.84957 9.38158 9.55074 8.83897C10.2519 8.29636 11.1134 8.00195 12 8.00195C12.8866 8.00195 13.7481 8.29636 14.4493 8.83897C15.1504 9.38158 15.6516 10.1417 15.874 10.9999H21V12.9999H15.874ZM12 13.9999C12.5304 13.9999 13.0391 13.7892 13.4142 13.4141C13.7893 13.039 14 12.5303 14 11.9999C14 11.4695 13.7893 10.9608 13.4142 10.5857C13.0391 10.2106 12.5304 9.9999 12 9.9999C11.4696 9.9999 10.9609 10.2106 10.5858 10.5857C10.2107 10.9608 10 11.4695 10 11.9999C10 12.5303 10.2107 13.039 10.5858 13.4141C10.9609 13.7892 11.4696 13.9999 12 13.9999Z" fill="#5A5A89" />
                                </svg>
                              </a>
                            </li>
                            <li>
                              <a>
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                  <path d="M15 5H17C17.5305 5 18.0392 5.21071 18.4142 5.58579C18.7893 5.96086 19 6.46957 19 7V15.17C19.6675 15.4059 20.2301 15.8702 20.5884 16.4808C20.9467 17.0914 21.0775 17.809 20.9578 18.5068C20.8382 19.2046 20.4757 19.8375 19.9344 20.2939C19.3931 20.7502 18.708 21.0005 18 21.0005C17.2921 21.0005 16.6069 20.7502 16.0657 20.2939C15.5244 19.8375 15.1619 19.2046 15.0422 18.5068C14.9225 17.809 15.0534 17.0914 15.4117 16.4808C15.7699 15.8702 16.3325 15.4059 17 15.17V7H15V10L10.5 6L15 2V5ZM5.00003 8.83C4.33254 8.59409 3.76994 8.1298 3.41167 7.51919C3.0534 6.90859 2.92254 6.19098 3.04222 5.49321C3.16189 4.79545 3.5244 4.16246 4.06566 3.70613C4.60692 3.2498 5.29208 2.99951 6.00003 2.99951C6.70798 2.99951 7.39314 3.2498 7.9344 3.70613C8.47566 4.16246 8.83817 4.79545 8.95784 5.49321C9.07752 6.19098 8.94666 6.90859 8.58839 7.51919C8.23012 8.1298 7.66752 8.59409 7.00003 8.83V15.17C7.66752 15.4059 8.23012 15.8702 8.58839 16.4808C8.94666 17.0914 9.07752 17.809 8.95784 18.5068C8.83817 19.2046 8.47566 19.8375 7.9344 20.2939C7.39314 20.7502 6.70798 21.0005 6.00003 21.0005C5.29208 21.0005 4.60692 20.7502 4.06566 20.2939C3.5244 19.8375 3.16189 19.2046 3.04222 18.5068C2.92254 17.809 3.0534 17.0914 3.41167 16.4808C3.76994 15.8702 4.33254 15.4059 5.00003 15.17V8.83ZM6.00003 7C6.26525 7 6.5196 6.89464 6.70714 6.70711C6.89467 6.51957 7.00003 6.26522 7.00003 6C7.00003 5.73478 6.89467 5.48043 6.70714 5.29289C6.5196 5.10536 6.26525 5 6.00003 5C5.73481 5 5.48046 5.10536 5.29292 5.29289C5.10539 5.48043 5.00003 5.73478 5.00003 6C5.00003 6.26522 5.10539 6.51957 5.29292 6.70711C5.48046 6.89464 5.73481 7 6.00003 7ZM6.00003 19C6.26525 19 6.5196 18.8946 6.70714 18.7071C6.89467 18.5196 7.00003 18.2652 7.00003 18C7.00003 17.7348 6.89467 17.4804 6.70714 17.2929C6.5196 17.1054 6.26525 17 6.00003 17C5.73481 17 5.48046 17.1054 5.29292 17.2929C5.10539 17.4804 5.00003 17.7348 5.00003 18C5.00003 18.2652 5.10539 18.5196 5.29292 18.7071C5.48046 18.8946 5.73481 19 6.00003 19ZM18 19C18.2652 19 18.5196 18.8946 18.7071 18.7071C18.8947 18.5196 19 18.2652 19 18C19 17.7348 18.8947 17.4804 18.7071 17.2929C18.5196 17.1054 18.2652 17 18 17C17.7348 17 17.4805 17.1054 17.2929 17.2929C17.1054 17.4804 17 17.7348 17 18C17 18.2652 17.1054 18.5196 17.2929 18.7071C17.4805 18.8946 17.7348 19 18 19Z" fill="#5A5A89" />
                                </svg>
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div className="multi-circle-chart-map-wrap where-visitors-live-map">
                      {map && <div ref={mapContainer3} className="map-container dashboard-map" />}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="row merchandise-trade-row">
            <div className="col col-12">
              <div className="card merchandise-trade-card">
                <div className="card-body">
                  <div class="card-title"><label for="">Demand by Merchandise Line within Trade Area</label></div>
                  <div className="card-content">
                    <div className="chart-wrapper">
                      <Chart
                        options={merchandiseTrade.options}
                        series={merchandiseTrade.series}
                        type="bar"
                        width="100%"
                        height="100%"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </section>
    </>
  )
}

// export default PropertyView;

const mapStateToProps = createStructuredSelector({
  theme: selectThemeSetting,
});

const mapDispatchToProps = (dispatch) => ({
  setThemeSetting: (theme) => dispatch(setThemeSetting(theme)),
});

export default connect(mapStateToProps, mapDispatchToProps)(PropertyView);