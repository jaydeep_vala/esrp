import React, { useRef, useEffect, useState } from "react";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { selectThemeSetting } from "../../redux/theme/theme.selector";
import { setThemeSetting } from "../../redux/theme/theme.action";
import mapboxgl from "!mapbox-gl"; // eslint-disable-line import/no-webpack-loader-syntax
import Input from "../control-component/input";
import Select from "../control-component/selectbox";
import "./MsaRanking.style.scss";
import Chart from "react-apexcharts";

const geojson = {
  type: "FeatureCollection",
  features: [
    {
      type: "Feature",
      geometry: {
        type: "Point",
        coordinates: [-77.032, 38.913],
      },
      properties: {
        title: "1",
        description: "",
      },
    },
    {
      type: "Feature",
      geometry: {
        type: "Point",
        coordinates: [-122.414, 37.776],
      },
      properties: {
        title: "2",
        description: "",
      },
    },
    {
      type: "Feature",
      geometry: {
        type: "Point",
        coordinates: [-100.414, 30.776],
      },
      properties: {
        title: "3",
        description: "",
      },
    },
    {
      type: "Feature",
      geometry: {
        type: "Point",
        coordinates: [-100.414, 40.776],
      },
      properties: {
        title: "4",
        description: "",
      },
    },
  ],
};

const MsaRanking = ({ theme, setThemeSetting }) => {
  const mapContainer = useRef(null);
  const map = useRef(null);
  let mapColor;
  const [lng, setLng] = useState(-77.55);
  const [lat, setLat] = useState(38.77);
  const [zoom, setZoom] = useState(4.5);
  const [location, setLocation] = useState();

  useEffect(() => {
    document.body.classList.remove("light-theme", "dark-theme");
    document.body.classList.add(theme.theme_color);

    if (theme.theme_color === "esrp-theme") {
      mapColor =
        "mapbox://styles/basalsmartsolutions/ckvtyjs1z2hcx14oyb5axlvlc";
      if (map.current) map.current.setStyle(mapColor);
    }
    if (theme.theme_color === "light-theme") {
      mapColor =
        "mapbox://styles/basalsmartsolutions/ckw232our0h7q14qs4h6yv2bx";
      if (map.current) map.current.setStyle(mapColor);
    }
    if (theme.theme_color === "dark-theme") {
      mapColor =
        "mapbox://styles/basalsmartsolutions/ckw219z4h1r7s14qtbw4fz3x8";
      if (map.current) map.current.setStyle(mapColor);
    }

    if (map.current) return;
    if (mapColor) {
      map.current = new mapboxgl.Map({
        container: mapContainer.current,
        style: mapColor,
        center: [lng, lat],
        zoom: zoom,
      });

      for (const feature of geojson.features) {
        // create a HTML element for each feature
        const el = document.createElement("div");
        el.className = "marker";

        // make a marker for each feature and add it to the map
        new mapboxgl.Marker(el)
          .setLngLat(feature.geometry.coordinates)
          .setPopup(
            new mapboxgl.Popup({ offset: 25 }) // add popups
              .setHTML(
                `<span class="title">${feature.properties.title}</span><span class="sub-title">${feature.properties.description}</span>`
              )
          )
          .addTo(map.current);
      }
    }
  }, [theme.theme_color]);

  const dataSet = {
    series: [56],
    options: {
      tooltip: {
        enabled: false,
      },
      chart: {
        height: "67px",
      },
      plotOptions: {
        radialBar: {
          hollow: {
            size: "90%",
          },
        },
      },
      colors: ["#5c86c1"],
      labels: [""],
    },
  };

  return (
    <>
      <section className="msa-ranking-sec">
        <div className="container">
          <div className="row title-filter-row">
            <div className="col col-6">
              <div className="breadcrumbs">
                <ul>
                  <li className="breadcrumb-item">
                    <a href="#!">Home</a>
                  </li>
                  <li className="breadcrumb-item">Dashboards</li>
                </ul>
              </div>
              <h3 className="page-title">MSA Ranking (Coverage Zone)</h3>
            </div>
            <div className="col col-6">
              <div className="analysis-filter-wrapper">
                <div className="analysis-filter-item">
                  <button className="btn rounded outline">Download</button>
                </div>
                <div className="analysis-filter-item location">
                  <Select
                    selectItem={location}
                    setSelectedItem={setLocation}
                    lableName="Location"
                    options={[
                      "Philadelphia, PA",
                      "Philadelphia, PA",
                      "Philadelphia, PA",
                      "Philadelphia, PA",
                    ]}
                    selectIcon={
                      <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M21 10C21 17 12 23 12 23C12 23 3 17 3 10C3 7.61305 3.94821 5.32387 5.63604 3.63604C7.32387 1.94821 9.61305 1 12 1C14.3869 1 16.6761 1.94821 18.364 3.63604C20.0518 5.32387 21 7.61305 21 10Z"
                          stroke="#5C86C1"
                          stroke-width="2"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                        />
                        <path
                          d="M12 13C13.6569 13 15 11.6569 15 10C15 8.34315 13.6569 7 12 7C10.3431 7 9 8.34315 9 10C9 11.6569 10.3431 13 12 13Z"
                          stroke="#5C86C1"
                          stroke-width="2"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                        />
                      </svg>
                    }
                  />
                </div>
              </div>
            </div>
          </div>

          <div className="row chart-row">
            <div className="col col-12">
              <div className="card coverage-zone-card">
                <div className="card-body">
                  <div className="card-title">
                    <label htmlFor="">Coverage Zone</label>
                    <span className="big-text">Philadelphia, PA</span>
                  </div>
                  <div className="card-content">
                    <div className="row circle-chart-items-wrapper">
                      <div className="col col-4">
                        <div className="circle-chart-item one">
                          <div className="left-side">
                            <div
                              className="icon"
                              style={{ backgroundColor: "#5c86c1" }}
                            >
                              <svg
                                width="32"
                                height="32"
                                viewBox="0 0 32 32"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                              >
                                <path
                                  d="M16.0001 29.3327C23.3639 29.3327 29.3334 23.3631 29.3334 15.9993C29.3334 8.63555 23.3639 2.66602 16.0001 2.66602C8.63628 2.66602 2.66675 8.63555 2.66675 15.9993C2.66675 23.3631 8.63628 29.3327 16.0001 29.3327Z"
                                  stroke="white"
                                  stroke-width="2"
                                  stroke-linecap="round"
                                  stroke-linejoin="round"
                                />
                                <path
                                  d="M16 24C20.4183 24 24 20.4183 24 16C24 11.5817 20.4183 8 16 8C11.5817 8 8 11.5817 8 16C8 20.4183 11.5817 24 16 24Z"
                                  stroke="white"
                                  stroke-width="2"
                                  stroke-linecap="round"
                                  stroke-linejoin="round"
                                />
                                <path
                                  d="M16.0002 18.6654C17.4729 18.6654 18.6668 17.4715 18.6668 15.9987C18.6668 14.5259 17.4729 13.332 16.0002 13.332C14.5274 13.332 13.3335 14.5259 13.3335 15.9987C13.3335 17.4715 14.5274 18.6654 16.0002 18.6654Z"
                                  stroke="white"
                                  stroke-width="2"
                                  stroke-linecap="round"
                                  stroke-linejoin="round"
                                />
                              </svg>
                            </div>
                            <div className="icon-content">
                              <label htmlFor="">Coverage Zone</label>
                              <h6>1 Day</h6>
                            </div>
                          </div>
                          <div className="circle-chart">
                            <div className="mixed-chart">
                              <Chart
                                options={dataSet.options}
                                series={dataSet.series}
                                type="radialBar"
                                width="100%"
                              />
                            </div>
                            <div className="chart-data">
                              <div className="chart-data-title">
                                <label htmlFor="">Reachable Population</label>
                              </div>
                              <div className="chart-price">
                                <h6>80,519,566</h6>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col col-4">
                        <div className="circle-chart-item two">
                          <div className="left-side">
                            <div
                              className="icon"
                              style={{ backgroundColor: "#3FB7F3" }}
                            >
                              <svg
                                width="32"
                                height="32"
                                viewBox="0 0 32 32"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                              >
                                <path
                                  d="M16.0001 29.3327C23.3639 29.3327 29.3334 23.3631 29.3334 15.9993C29.3334 8.63555 23.3639 2.66602 16.0001 2.66602C8.63628 2.66602 2.66675 8.63555 2.66675 15.9993C2.66675 23.3631 8.63628 29.3327 16.0001 29.3327Z"
                                  stroke="white"
                                  stroke-width="2"
                                  stroke-linecap="round"
                                  stroke-linejoin="round"
                                />
                                <path
                                  d="M16 24C20.4183 24 24 20.4183 24 16C24 11.5817 20.4183 8 16 8C11.5817 8 8 11.5817 8 16C8 20.4183 11.5817 24 16 24Z"
                                  stroke="white"
                                  stroke-width="2"
                                  stroke-linecap="round"
                                  stroke-linejoin="round"
                                />
                                <path
                                  d="M16.0002 18.6654C17.4729 18.6654 18.6668 17.4715 18.6668 15.9987C18.6668 14.5259 17.4729 13.332 16.0002 13.332C14.5274 13.332 13.3335 14.5259 13.3335 15.9987C13.3335 17.4715 14.5274 18.6654 16.0002 18.6654Z"
                                  stroke="white"
                                  stroke-width="2"
                                  stroke-linecap="round"
                                  stroke-linejoin="round"
                                />
                              </svg>
                            </div>
                            <div className="icon-content">
                              <label htmlFor="">Coverage Zone</label>
                              <h6>1 Day</h6>
                            </div>
                          </div>
                          <div className="circle-chart">
                            <div className="mixed-chart">
                              <Chart
                                options={dataSet.options}
                                series={dataSet.series}
                                type="radialBar"
                                width="100%"
                              />
                            </div>
                            <div className="chart-data">
                              <div className="chart-data-title">
                                <label htmlFor="">Reachable Population</label>
                              </div>
                              <div className="chart-price">
                                <h6>80,519,566</h6>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col col-4">
                        <div className="circle-chart-item three">
                          <div className="left-side">
                            <div
                              className="icon"
                              style={{ backgroundColor: "#81CAB2" }}
                            >
                              <svg
                                width="32"
                                height="32"
                                viewBox="0 0 32 32"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                              >
                                <path
                                  d="M16.0001 29.3327C23.3639 29.3327 29.3334 23.3631 29.3334 15.9993C29.3334 8.63555 23.3639 2.66602 16.0001 2.66602C8.63628 2.66602 2.66675 8.63555 2.66675 15.9993C2.66675 23.3631 8.63628 29.3327 16.0001 29.3327Z"
                                  stroke="white"
                                  stroke-width="2"
                                  stroke-linecap="round"
                                  stroke-linejoin="round"
                                />
                                <path
                                  d="M16 24C20.4183 24 24 20.4183 24 16C24 11.5817 20.4183 8 16 8C11.5817 8 8 11.5817 8 16C8 20.4183 11.5817 24 16 24Z"
                                  stroke="white"
                                  stroke-width="2"
                                  stroke-linecap="round"
                                  stroke-linejoin="round"
                                />
                                <path
                                  d="M16.0002 18.6654C17.4729 18.6654 18.6668 17.4715 18.6668 15.9987C18.6668 14.5259 17.4729 13.332 16.0002 13.332C14.5274 13.332 13.3335 14.5259 13.3335 15.9987C13.3335 17.4715 14.5274 18.6654 16.0002 18.6654Z"
                                  stroke="white"
                                  stroke-width="2"
                                  stroke-linecap="round"
                                  stroke-linejoin="round"
                                />
                              </svg>
                            </div>
                            <div className="icon-content">
                              <label htmlFor="">Coverage Zone</label>
                              <h6>1 Day</h6>
                            </div>
                          </div>
                          <div className="circle-chart">
                            <div className="mixed-chart">
                              <Chart
                                options={dataSet.options}
                                series={dataSet.series}
                                type="radialBar"
                                width="100%"
                              />
                            </div>
                            <div className="chart-data">
                              <div className="chart-data-title">
                                <label htmlFor="">Reachable Population</label>
                              </div>
                              <div className="chart-price">
                                <h6>80,519,566</h6>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="row msa-map-inner-row">
                      <div className="col col-12">
                        <div className="map-wrapper multi-circle-chart-map-wrap">
                          {map && (
                            <div
                              ref={mapContainer}
                              className="map-container dashboard-map"
                            />
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

const mapStateToProps = createStructuredSelector({
  theme: selectThemeSetting,
});

const mapDispatchToProps = (dispatch) => ({
  setThemeSetting: (theme) => dispatch(setThemeSetting(theme)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MsaRanking);
