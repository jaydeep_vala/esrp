import React, { useRef, useEffect, useState } from "react";
import { connect } from "react-redux";
import Chart from "react-apexcharts";
import Select from "../control-component/selectbox";
import "./LaborSustainability.style.scss";
import Input from "../control-component/input";

const LaborSustainability = () => {
  const [segmentationWeight, setSegmentationWeight] = useState();
  const [laborWeight, setLaborWeight] = useState();
  const [showFilter, setShowFilter] = useState(false);

  // FOR BAR CHART
  const ActivelyEmployed = {
    series: [
      {
        data: [746165, 393555, 234881, 201881, 103481, 70000],
      },
    ],
    options: {
      chart: {
        type: "bar",
        toolbar: {
          show: false,
        },
      },
      labels: ["Apples", "Oranges", "Berries"],
      plotOptions: {
        bar: {
          borderRadius: 0,
          barHeight: "70%",
          distributed: true,
          horizontal: true,
          dataLabels: {
            position: "bottom",
          },
        },
      },
      legend: {
        show: false,
      },
      colors: ["#5C86C1", "#3FB7F3", "#81CAB2"],
      dataLabels: {
        enabled: true,
        textAnchor: "start",
        style: {
          colors: ["#fff"],
        },
        formatter: function (val, opt) {
          var amount = parseInt(val);
          if (amount > 1000 && amount < 230000) {
            amount = amount / 1000;
            amount = Math.round(amount).toString();
            amount = amount + "k";
          } else {
            amount =
              amount >= 1000
                ? amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                : amount;
          }

          return amount;
        },
        offsetX: 0,
        dropShadow: {
          enabled: false,
        },
      },
      stroke: {
        width: 0,
        colors: ["#fff"],
      },
      xaxis: {
        labels: {
          show: true,
          formatter: function (val, opt) {
            var amount = parseInt(val);
            if (amount > 1000 && amount > 150000) {
              amount = amount / 1000;
              amount = Math.round(amount).toString();
              amount = amount + "k";
            }
            return amount;
          },
        },
        categories: [
          "Los Angeles, CA",
          "Phoenix, AZ",
          "Philadelphia, PA",
          "Las Vegas, NV",
          "Salt Lake City, NV",
          "Reno, NV",
        ],
        axisBorder: {
          show: false,
          color: "#78909C",
          height: 1,
          width: "100%",
          offsetX: 0,
          offsetY: 0,
        },
        axisTicks: {
          show: false,
        },
      },
      yaxis: {
        labels: {
          show: true,
        },
        axisBorder: {
          show: false,
        },
        labels: {
          show: true,
          align: "right",
          minWidth: 100,
          // maxWidth: 100,
          offsetX: 0,
          offsetY: 0,
          rotate: 0,
        },
      },
      grid: {
        show: true,
        borderColor: "#E6E8ED",
        strokeDashArray: 0,
        position: "back",
        xaxis: {
          lines: {
            show: false,
          },
        },
        yaxis: {
          lines: {
            show: true,
          },
        },
      },
      tooltip: {
        theme: "light",
        custom: function ({ series, seriesIndex, dataPointIndex, w }) {
          var dataPoint = series[seriesIndex][dataPointIndex];
          return dataPoint >= 1000
            ? dataPoint.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
            : dataPoint;
        },
      },
    },
  };

  const PastLaborGrothChart = {
    series: [
      {
        data: [2272, -500, -780, -1900, -2001, -7584],
      },
    ],
    options: {
      chart: {
        type: "bar",
        toolbar: {
          show: false,
        },
      },
      labels: ["Apples", "Oranges", "Berries"],
      plotOptions: {
        bar: {
          borderRadius: 0,
          barHeight: "70%",
          distributed: true,
          horizontal: true,
          dataLabels: {
            position: "bottom",
          },
        },
      },
      legend: {
        show: false,
      },
      colors: ["#5C86C1", "#3FB7F3", "#81CAB2"],
      dataLabels: {
        enabled: true,
        textAnchor: "start",
        style: {
          colors: ["#fff"],
        },
        formatter: function (val, opt) {
          var amount = parseInt(val);
          if (amount > 1000 && amount < 230000) {
            amount = amount / 1000;
            amount = Math.round(amount).toString();
            amount = amount + "k";
          } else {
            amount =
              amount >= 1000
                ? amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                : amount;
          }

          return amount;
        },
        offsetX: 0,
        dropShadow: {
          enabled: false,
        },
      },
      stroke: {
        width: 0,
        colors: ["#fff"],
      },
      xaxis: {
        labels: {
          show: true,
          formatter: function (val, opt) {
            var amount = parseInt(val);
            if (amount > 1000 && amount > 150000) {
              amount = amount / 1000;
              amount = Math.round(amount).toString();
              amount = amount + "k";
            }
            return amount;
          },
        },
        categories: [
          "Los Angeles, CA",
          "Phoenix, AZ",
          "Philadelphia, PA",
          "Las Vegas, NV",
          "Salt Lake City, NV",
          "Reno, NV",
        ],
        axisBorder: {
          show: false,
          color: "#78909C",
          height: 1,
          width: "100%",
          offsetX: 0,
          offsetY: 0,
        },
        axisTicks: {
          show: false,
        },
      },
      yaxis: {
        labels: {
          show: true,
        },
        axisBorder: {
          show: false,
        },
        labels: {
          show: true,
          align: "right",
          minWidth: 100,
          // maxWidth: 100,
          offsetX: 0,
          offsetY: 0,
          rotate: 0,
        },
      },
      grid: {
        show: true,
        borderColor: "#E6E8ED",
        strokeDashArray: 0,
        position: "back",
        xaxis: {
          lines: {
            show: false,
          },
        },
        yaxis: {
          lines: {
            show: true,
          },
        },
      },
      tooltip: {
        theme: "light",
        custom: function ({ series, seriesIndex, dataPointIndex, w }) {
          var dataPoint = series[seriesIndex][dataPointIndex];
          return dataPoint >= 1000
            ? dataPoint.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
            : dataPoint;
        },
      },
    },
  };

  return (
    <>
      <section className="labor-supply-sec">
        <div className="container">
          <div className="row title-filter-row">
            <div className="col col-6">
              <div className="breadcrumbs">
                <ul>
                  <li className="breadcrumb-item">
                    <a href="#!">Home</a>
                  </li>
                  <li className="breadcrumb-item">
                    <a href="#!">Dashboards</a>
                  </li>
                  <li className="breadcrumb-item">MSA Ranking</li>
                </ul>
              </div>
              <h3 className="page-title">Labor Sustainability</h3>
            </div>
            <div className="col col-6">
              <div className="analysis-filter-wrapper">
                <div className="analysis-filter-item">
                  <button className="btn rounded outline">Download</button>
                </div>
                <div className="analysis-filter-item">
                  <button
                    className="btn rounded primary"
                    onClick={() => setShowFilter(true)}
                  >
                    Filters
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="row chart-row">
            <div className="col col-4">
              <div className="card labor-year-growth-card">
                <div className="card-body">
                  <div className="card-title">
                    <label htmlFor="">5 Year Projected Growth</label>
                    {/* <span className="big-text">Actively Employed Workers</span> */}
                  </div>
                  <div className="card-content">
                    <div className="chart-wrapper">
                      <Chart
                        options={ActivelyEmployed.options}
                        series={ActivelyEmployed.series}
                        type="bar"
                        width="100%"
                        height="100%"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col col-4">
              <div className="card labor-growth-card">
                <div className="card-body">
                  <div className="card-title">
                    <label htmlFor="">Past Year’s Labor Growth</label>
                  </div>
                  <div className="card-content">
                    <div className="chart-wrapper">
                      <Chart
                        options={PastLaborGrothChart.options}
                        series={PastLaborGrothChart.series}
                        type="bar"
                        width="100%"
                        height="100%"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col col-4">
              <div className="card labor-poulation-card">
                <div className="card-body">
                  <div className="card-title">
                    <label htmlFor="">Total Poulation Growth</label>
                  </div>
                  <div className="card-content">
                    <div className="chart-wrapper">
                      <Chart
                        options={ActivelyEmployed.options}
                        series={ActivelyEmployed.series}
                        type="bar"
                        width="100%"
                        height="100%"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <div
        className={`${showFilter ? "show" : ""} overlay`}
        onClick={() => setShowFilter(false)}
      ></div>

      <div
        className={`${showFilter ? "show" : ""} labor-filter-sidebar`}
        key={showFilter}
      >
        <div className="sidebar-inner">
          <div className="sidebar-item-btn-close">
            <span onClick={() => setShowFilter(false)}>
              <svg
                width="14"
                height="15"
                viewBox="0 0 14 15"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M13 1.00195L1 13.002"
                  stroke="#111128"
                  stroke-width="2"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
                <path
                  d="M1 1.00195L13 13.002"
                  stroke="#111128"
                  stroke-width="2"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
              </svg>
            </span>
            {/* <button className="btn primary hide-trigger" onClick={() => setShowFilter(false)}>Hide Filters</button> */}
          </div>
          <div className="sidebar-item-wrapper">
            <div className="sidebar-item">
              <div className="sidebar-title">
                <h4>Filters</h4>
                <span className="body-2">Total must equal 100%</span>
              </div>
              <div className="input-item">
                <Input
                  name="LaborSupply"
                  id="LaborSupply"
                  type="text"
                  lable="5 Year Projected Growth"
                />
              </div>
              <div className="input-item">
                <Input
                  name="LaborCost"
                  id="LaborCost"
                  type="text"
                  lable="Past Year’s Labor Growth"
                />
              </div>
              <div className="input-item">
                <Input
                  name="LaborSustainability"
                  id="LaborSustainability"
                  type="text"
                  lable="Total Poulation Growth"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default LaborSustainability;
