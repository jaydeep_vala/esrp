import React, { useEffect, useState } from "react";
import Chart from "react-apexcharts";

// FOR BAR CHART
const defaultOptions = {

        chart: {
            type: 'bar',
            toolbar: {
                show: false,
            },
            tooltip: {
                show: true,
            }
        },
        grid: {
            borderColor: 'transparent',
        },
        plotOptions: {
            bar: {
                startingShape: 'rounded',
                borderRadius: 16,
                barHeight: '80%',
                distributed: true,
                horizontal: true,
                dataLabels: {
                    position: 'bottom'
                },
            }
        },
        colors: [
            '#5C86C1',
            '#5C86C1',
            '#5C86C1',
            '#5C86C1',
            '#5C86C1',
            '#5C86C1',
            '#5C86C1',
            '#5C86C1',
            '#5C86C1',
            '#3FB7F3',
            '#3FB7F3',
            '#3FB7F3',
            '#3FB7F3',
            '#3FB7F3',
            '#81CAB2',
            '#81CAB2',
            '#81CAB2',
            '#81CAB2',
        ],
        dataLabels: {
            enabled: true,
            textAnchor: 'start',
            style: {
                colors: ['#fff']
            },
            formatter: function (val, opt) {
                var amount = parseInt(val);
                if (amount > 1000 && amount < 100000) {
                    amount = amount / 1000;
                    amount = Math.round(amount).toString();
                    amount = amount + 'k';
                }
                return amount
            },
            offsetX: 0,
            dropShadow: {
                enabled: true
            }
        },
        stroke: {
            width: 1,
            colors: ['transparent']
        },
        legend: {
            show: false,
        },
        xaxis: {
            categories: [
                '0 - 4',
                '5 - 9',
                '10 - 14',
                '15 - 19',
                '20 - 24',
                '25 - 29',
                '30 - 34',
                '35 - 39',
                '40 - 44',
                '45 - 49',
                '50 - 54',
                '55 - 59',
                '60 - 64',
                '65 - 69',
                '70 - 74',
                '75 - 79',
                '80 -84',
                '85+',
            ],
            labels: {
                show: false
            }
        },
        yaxis: {
            categories: [
                '0 - 4',
                '5 - 9',
                '10 - 14',
                '15 - 19',
                '20 - 24',
                '25 - 29',
                '30 - 34',
                '35 - 39',
                '40 - 44',
                '45 - 49',
                '50 - 54',
                '55 - 59',
                '60 - 64',
                '65 - 69',
                '70 - 74',
                '75 - 79',
                '80 -84',
                '85+',
            ],
            labels: {
                show: true
            },
            opposite: true,
        },
        tooltip: {
            custom: function ({ series, seriesIndex, dataPointIndex, w }) {
                var dataPoint = series[seriesIndex][dataPointIndex];
                return dataPoint >= 1000 ? '$' + dataPoint.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : '$' + dataPoint
            }
        },
        noData: {
            text: 'Loading...'
        }
};

const defaultSeries = [
    {
        name: "Age",
        data: [],
    },
];
const BarChart = ({ data }) => {
    const [options, setOptions] = useState(defaultOptions);
    const [series, setSeries] = useState(defaultSeries);
    useEffect(() => {
        if (data) {
            if (data.label) {
                setOptions({
                    ...options,
                    xaxis: { ...options.xaxis, categories: data.label },
                    yaxis: { ...options.yaxis, categories: data.label },
                });
            }
            if (data.series) {
                setSeries([
                    {
                        name: "Sales",
                        data: data.series,
                    },
                ]);
            }
        }
    }, [data]);

    return (
        <>
            {series &&
                <Chart
                    options={options}
                    series={series}
                    type="bar"
                    width="100%"
                    height="100%"
                />
            }
        </>
    );
};
export default BarChart;




// import React, { useEffect, useState } from "react";
// import Chart from "react-apexcharts";

// // FOR BAR CHART
// const defaultOptions = {
//               chart: {
//                 type: 'bar',
//                 height: 350
//               },
//               plotOptions: {
//                 bar: {
//                   borderRadius: 4,
//                   horizontal: true,
//                 }
//               },
//               dataLabels: {
//                 enabled: false
//               },
//               xaxis: {
//                 categories: ['South Korea', 'Canada', 'United Kingdom', 'Netherlands', 'Italy', 'France', 'Japan',
//                   'United States', 'China', 'Germany'
//                 ],
//               }
// };

// const defaultSeries = [{
//     data: [400, 430, 448, 470, 540, 580, 690, 1100, 1200, 1380]
// }];
// const data1  ={
          
//     series: [{
//       data: [400, 430, 448, 470, 540, 580, 690, 1100, 1200, 1380]
//     }],
//     options: {
//       chart: {
//         type: 'bar',
//         height: 350
//       },
//       plotOptions: {
//         bar: {
//           borderRadius: 4,
//           horizontal: true,
//         }
//       },
//       dataLabels: {
//         enabled: false
//       },
//       xaxis: {
//         categories: ['South Korea', 'Canada', 'United Kingdom', 'Netherlands', 'Italy', 'France', 'Japan',
//           'United States', 'China', 'Germany'
//         ],
//       }
//     },
  
  
//   };
// const BarChart = ({ data }) => {
//     const [options, setOptions] = useState(defaultOptions);
//     const [series, setSeries] = useState(defaultSeries);
//     // useEffect(() => {
//     //     if (data) {
//     //         if (data.label) {
//     //             setOptions({
//     //                 ...options,
//     //                 xaxis: { ...options.xaxis, categories: data.label },
//     //                 yaxis: { ...options.yaxis, categories: data.label },
//     //             });
//     //         }
//     //         if (data.series) {
//     //             setSeries([
//     //                 {
//     //                     name: "Sales",
//     //                     data: data.series,
//     //                 },
//     //             ]);
//     //         }
//     //     }
//     // }, [data]);

//     return (
//         <>
//                 <Chart
//                     options={data1.options}
//                     series={data1.series}
//                     type="bar"
//                     width="100%"
//                     height="100%"
//                 />
//         </>
//     );
// };
// export default BarChart;
