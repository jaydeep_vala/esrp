import React, { useEffect, useState } from "react";
import Chart from "react-apexcharts";

const defaultOptions = {
        chart: {
        },
        plotOptions: {
            pie: {
                donut: {
                    
                }
            }
        },
        colors: ['#8B79D5', '#81CAB2', '#5C86C1'],
        labels: ['High School or less', 'Some College', 'Bachelors / Grad'],
        stroke: {
            show: false,
            width: 0,
        },
        dataLabels: {
            enabled: false
        },
        tooltip: {
            enabled: false
        },
        legend: {
            formatter: function (seriesName, opts) {
                return "<span class='custom-legend' style='background-color:" + opts.w.globals.colors[opts.seriesIndex] + "'>" + seriesName + "</span><span>" + opts.w.globals.series[opts.seriesIndex] + "%</span></div>";
            }
        },
        responsive: [{
            breakpoint: 480,
            options: {
                chart: {
                    width: 200
                },
                legend: {
                    position: 'bottom',
                    formatter: function (seriesName, opts) {
                        return "<span class='custom-legend' style='background-color:" + opts.w.globals.colors[opts.seriesIndex] + "'>" + seriesName + "</span><span>" + opts.w.globals.series[opts.seriesIndex] + "%</span></div>";
                    }
                }
            }
        }],
    noData: {
        text: 'Loading...'
    }
};

const defaultSeries = [
    {
        name: "Education Data",
        data: [],
    },
];
const RedialChart = ({ data }) => {
    const [options, setOptions] = useState(defaultOptions);
    const [series, setSeries] = useState(defaultSeries);
    useEffect(() => {
        if (data) {
            if (data.label) {
                setOptions({
                    ...options,
                    labels: data.label
                });
            }
            if (data.series) {
                // setSeries([
                //     {
                //         name: "Sales",
                //         data: data.series,
                //     },
                // ]);
                setSeries(data.series.map(item => Number(item.replace('%',''))))

            }
        }
    }, [data]);
    
    return (
        <>
            <Chart
            options={options}
            series={series}
            type="donut"
            width="100%"
            height="132px"
            />
            </>
    );
};
export default RedialChart;
