import React, { useEffect, useState } from "react";

const CardChart = ({ data }) => {
    
    
    return (
        <ul>
            {
                data.map(val => (
                    <li key={val.label}>
                        <span className="name">{val.label}</span>
                        <span className="value">{val.series}</span>
                    </li>
                ))
            }
        </ul>
    );
};
export default CardChart;
