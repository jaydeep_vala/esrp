import React, { useEffect, useState } from "react";
import Chart from "react-apexcharts";

const defaultOptions = {
    tooltip: {
        shared: true,
        intersect: false,
        x: {
            show: true,
        },
        y: {
            show: true,
        },
    },
    colors: ['#81CAB2'],
    chart: {
        stacked: true,
        toolbar: {
            show: false
        },
        zoom: {
            enabled: false
        }
    },
    legend: {
        show: false
    },
    dataLabels: {
        enabled: false
    },
    grid: {
        show: false
    },
    plotOptions: {
        bar: {
            // horizontal: false,
            columnWidth: '10%',
            borderRadius: 7,
            dataLabels: {
                position: 'top', // top, center, bottom
            },
            ranges: [{
                from: 20,
                to: 100,
                color: '#F15B46'
            }]
        },
    },
    xaxis: {
        categories: [
            ['1 Adult', '0 Kids'],
            ['1 Adult', '1 Kid'],
            ['1 Adult', '2 Kids'],
            ['1 Adult', '3 Kids'],
            ['2 Adults', '(1 Working)', '0 Kids'],
            ['2 Adults', '(2 Working)', '1 Kid'],
            ['2 Adults', '(1 Working)', '2 Kids'],
            ['2 Adults', '(1 Working)', '3 Kids'],
            ['2 Adults', '(Both Working)', '0 Kids'],
            ['2 Adults', '(Both Working)', '1 Kid'],
            ['2 Adults', '(Both Working)', '2 Kid'],
            ['2 Adults', '(Both Working)', '3 Kid']

        ],
        // show: false,
        labels: {
            show: true,
        },
        lines: {
            show: false
        },
        axisTicks: {
            show: false
        }
    },
    yaxis: {
        labels: {
            show: true,
        },
        lines: {
            show: false
        }
    },

    noData: {
        text: 'Loading...'
    }
}

const defaultSeries = [
    {
        name: "Sales",
        data: [],
    },
];
const ColumnChart = ({ data }) => {
    const [options, setOptions] = useState(defaultOptions);
    const [series, setSeries] = useState(defaultSeries);
    useEffect(() => {
        if (data) {
            if (data.label) {
                setOptions({
                    ...options,
                    xaxis: { ...options.xaxis, categories: data.label },
                });
            }
            if (data.series) {
                setSeries([
                    {
                        name: "growth",
                        data: data.series,
                    },
                ]);
            }
        }
    }, [data]);
    
    return (
        <Chart
            options={options}
            series={series}
            type="bar"
            width="100%"
            height="100%"
        />
    );
};
export default ColumnChart;
