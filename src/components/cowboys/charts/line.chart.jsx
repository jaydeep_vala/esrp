import React, { useEffect, useState } from "react";
import Chart from "react-apexcharts";

const defaultOptions = {
    
    plotOptions: {
        pie: {
            expandOnClick: false
        }
    },
    chart: {
        type: 'line',
        dropShadow: {
            enabled: false,
            color: '#000',
            top: 18,
            left: 7,
            blur: 10,
            opacity: 0,
        },
        toolbar: {
            show: true,
            tools: {
                download: false,
                selection: true,
                zoomin: '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M11 11V5H13V11H19V13H13V19H11V13H5V11H11Z" fill="#5A5A89"></path></svg>',
                zoomout: '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M11 11H13H19V13H13H11H5V11H11Z" fill="#5A5A89"></path></svg>',
                zoom: false,
                pan: '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M16 9V6L20 10L16 14V11H11V16H14L10 20L6 16H9V11H4V14L0 10L4 6V9H9V4H6L10 0L14 4H11V9H16Z" fill="#5A5A89"></path></svg>',
                reset: false,
            },
        }
    },
    colors: ['#5C86C1'],
    dataLabels: {
        enabled: true,
        formatter: function (value) {
            var dataPoint = Math.round(value);
            return dataPoint >= 1000 ? '$' + dataPoint : '$' + dataPoint
        }
    },
    stroke: {
        curve: 'smooth',
    },
    grid: {
        borderColor: 'transparent',
        column: {
            colors: ['#F4F5FB', 'transparent'], // takes an array which will be repeated on columns
            opacity: 0.5,
        },
    },
    markers: {
        size: 1,
    },
    xaxis: {
        categories: ['2016', '2017', '2018', '2019', '2020', '2021', '2022', '2023', '2024', '2025', '2026'],
    },
    yaxis: {
        // min: 5,
        // max: 40,
    },
    legend: {
        position: 'top',
        horizontalAlign: 'right',
        floating: true,
        offsetY: -25,
        offsetX: -5,
    },
    noData: {
        text: 'Loading...'
    }
};

const defaultSeries = [
    {
        name: "growth",
        data: [],
    },
];
const LineChart = ({ data }) => {
    const [options, setOptions] = useState(defaultOptions);
    const [series, setSeries] = useState(defaultSeries);
    useEffect(() => {
        if (data) {
            if (data.label) {
                setOptions({
                    ...options,
                    xaxis: { ...options.xaxis, categories: data.label },
                });
            }
            if (data.series) {
                setSeries([
                    {
                        name: "growth",
                        data: data.series,
                    },
                ]);
            }
        }
    }, [data]);

    return (
        <Chart
            options={options}
            series={series}
            type='line'
            width='100%'
            height="100%"
        />
    );
};
export default LineChart;
