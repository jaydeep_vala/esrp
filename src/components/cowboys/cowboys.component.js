import React, { useRef, useEffect, useState } from "react";
import MapDistance from "../map-distance/MapDistance";
import MapControl from "../map-control/MapControl";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { selectThemeSetting } from "../../redux/theme/theme.selector";
import { setThemeSetting } from "../../redux/theme/theme.action";
import mapboxgl from "!mapbox-gl"; // eslint-disable-line import/no-webpack-loader-syntax
import Select from "../control-component/selectbox";
import {
  dropdwonData,
  mapChart,
  marketOverview,
  marketAgeDistribution,
  marketEducation,
  marketOccupationWages,
  marketOccupationOverview,
  marketOccupationGrowth,
  marketRaceDistribution,
} from "../../services/dashboards/cowboy.service";
import BarChart from "./charts/bar.chart";
import ColumnChart from "./charts/column.chart";
import LineChart from "./charts/line.chart";
import RedialChart from "./charts/redial.chart";
import RedialChart2 from "./charts/redial2.chart";
import CardChart from "./charts/card.chart";
import "./cowboys.style.scss";
mapboxgl.accessToken =
  "pk.eyJ1IjoiYmFzYWxzbWFydHNvbHV0aW9ucyIsImEiOiJja3ZpZ2NtanNjazA3MnZuemt4ZnF6b2FoIn0.0oul5wnWu-7L_2HB0PTzCg";

const geojson = {
  type: "FeatureCollection",
  features: [
    {
      type: "Feature",
      geometry: {
        type: "Point",
        coordinates: [-77.032, 38.913],
      },
      properties: {
        title: "1",
        description: "",
      },
    },
  ],
};

const CowBoys = ({ theme, setThemeSetting }) => {
  const [data, setData] = useState({});
  const [dropDownOption, setDropDownOption] = useState([]);

  const getDashboardData = async () => {
    var data = JSON.stringify({
      drive_time_id: 3,
    });
    var occupationData = JSON.stringify({
      drive_time_id: 3,
      occupation: occupation.name,
    });
    const dropdwonDatafetch = await dropdwonData(data);
    const mapData = await mapChart(data);
    const marketOverviewData = await marketOverview(data);
    const marketAgeDistributionData = await marketAgeDistribution(data);
    const marketEducationData = await marketEducation(data);
    const marketOccupationWagesData = await marketOccupationWages(
      occupationData
    );
    const marketRaceDistributionData = await marketRaceDistribution(data);
    const marketOccupationOverviewData = await marketOccupationOverview(
      occupationData
    );
    const marketOccupationGrowthData = await marketOccupationGrowth(
      occupationData
    );

    const tempData = {};
    if (mapData) {
      tempData.mapData = mapData;
    }
    if (dropdwonDatafetch) {
      setDropDownOption(dropdwonDatafetch.data);
    }
    if (marketOverviewData) {
      let marketData = marketOverviewData.data.map((a) => {
        return { series: a.label_value, label: a.label_name };
      });
      tempData.marketOverviewData = marketData;
    }
    if (marketAgeDistributionData) {
      let series = marketAgeDistributionData.data.chart_data.map((a) => {
        return a.label_value;
      });
      let label = marketAgeDistributionData.data.chart_data.map((a) => {
        return a.label_name;
      });

      tempData.marketAgeDistributionData = {
        series: series,
        label: label,
      };
    }
    if (marketEducationData) {
      let series = marketEducationData.data.map((a) => {
        return a.education_percent;
      });
      let label = marketEducationData.data.map((a) => {
        return a.education_name;
      });
      tempData.marketEducationData = {
        series: series,
        label: label,
      };
    }
    if (marketOccupationWagesData) {
      let series = marketOccupationWagesData.data.chart_data.map((a) => {
        return a.label_value;
      });
      let label = marketOccupationWagesData.data.chart_data.map((a) => {
        return a.label_name;
      });
      tempData.marketOccupationWagesData = {
        series: series,
        label: label,
      };
    }
    if (marketOccupationOverviewData) {
      let overView = marketOccupationOverviewData.data.chart_data.map((a) => {
        return { name: a.label_name, value: a.label_value };
      });
      tempData.marketOccupationOverviewData = overView;
    }
    if (marketOccupationGrowthData) {
      let series = marketOccupationGrowthData.data.chart_data.map((a) => {
        return a.label_value;
      });
      let label = marketOccupationGrowthData.data.chart_data.map((a) => {
        return a.label_name;
      });
      tempData.marketOccupationGrowthData = {
        series: series,
        label: label,
      };
    }
    if (marketRaceDistributionData) {
      let series = marketRaceDistributionData.data.race_data.map((a) => {
        return a.race_percent;
      });
      let label = marketRaceDistributionData.data.race_data.map((a) => {
        return a.race_name;
      });
      tempData.marketRaceDistributionData = {
        series: series,
        label: label,
      };
    }
    setData(tempData);

    document.getElementsByClassName("dynamic-text")[0].innerHTML =
      occupation.name + '<span className="up">▲10%</span>';
    document.getElementsByClassName("dynamic-label")[0].textContent =
      "Growth of " + occupation.name;
  };

  const mapContainer = useRef(null);
  const map = useRef(null);
  let mapColor;
  const [lng, setLng] = useState(-77.55);
  const [lat, setLat] = useState(38.77);
  const [zoom, setZoom] = useState(4.5);
  const [occupation, setOccupation] = useState({ id: "", name: "Lawyers" });

  useEffect(() => {
    if (occupation.name) {
      getDashboardData();
    }
  }, [occupation]);

  useEffect(() => {
    getDashboardData();

    document.body.classList.remove("light-theme", "dark-theme");
    document.body.classList.add(theme.theme_color);

    if (theme.theme_color === "esrp-theme") {
      mapColor =
        "mapbox://styles/basalsmartsolutions/ckvtyjs1z2hcx14oyb5axlvlc";
      if (map.current) map.current.setStyle(mapColor);
    }
    if (theme.theme_color === "light-theme") {
      mapColor =
        "mapbox://styles/basalsmartsolutions/ckw232our0h7q14qs4h6yv2bx";
      if (map.current) map.current.setStyle(mapColor);
    }
    if (theme.theme_color === "dark-theme") {
      mapColor =
        "mapbox://styles/basalsmartsolutions/ckw219z4h1r7s14qtbw4fz3x8";
      if (map.current) map.current.setStyle(mapColor);
    }

    if (map.current) return;
    if (mapColor) {
      map.current = new mapboxgl.Map({
        container: mapContainer.current,
        style: mapColor,
        center: [lng, lat],
        zoom: zoom,
      });

      map.current.addControl(new mapboxgl.NavigationControl());
      map.current.addControl(
        new mapboxgl.GeolocateControl({
          positionOptions: {
            enableHighAccuracy: true,
          },
          trackUserLocation: true,
          showUserHeading: true,
        })
      );

      for (const feature of geojson.features) {
        // create a HTML element for each feature
        const el = document.createElement("div");
        el.className = "marker";

        // make a marker for each feature and add it to the map
        new mapboxgl.Marker(el)
          .setLngLat(feature.geometry.coordinates)
          .setPopup(
            new mapboxgl.Popup({ offset: 25 }) // add popups
              .setHTML(
                `<span className="title">${feature.properties.title}</span><span className="sub-title">${feature.properties.description}</span>`
              )
          )
          .addTo(map.current);
      }
    }
  }, [theme.theme_color]);

  return (
    <>
      <section className="cowboys-sec">
        <div className="container">
          <div className="row cowboys-title-filter-row">
            <div className="col col-6">
              <div className="breadcrumbs">
                <ul>
                  <li className="breadcrumb-item">
                    <a href="#!">Home</a>
                  </li>
                  <li className="breadcrumb-item">Dashboards</li>
                </ul>
              </div>
              <h3 className="page-title">1 COWBOYS WAY, FRISCO TX</h3>
            </div>
            <div className="col col-6">
              <div className="cowboys-filter-btn-select">
                <div className="inner">
                  <div>
                    <button className="btn primary rounded">Details</button>
                  </div>
                  <div>
                    <button className="btn rounded outline">Download</button>
                  </div>
                  <div>
                    {console.log({ dropDownOption })}
                    <Select
                      selectItem={occupation.name}
                      setSelectedItem={setOccupation}
                      lableName="Select Occupation"
                      options={dropDownOption}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row map-informer-row">
            <div className="col col-3">
              <div className="card cowboys-informer-card">
                <div className="card-body">
                  <label className="title">
                    The average salary for Lawyer is
                  </label>
                  <h3>$117,355</h3>
                  <span>in Dallas-Fort Worth, TX</span>
                </div>
              </div>

              <div className="card market-overview-card">
                <div className="card-body">
                  <div className="card-title">
                    <label htmlFor="">Market Overview</label>
                  </div>
                  {console.log(
                    "data.marketOverviewData",
                    data.marketOverviewData
                  )}
                  {data.marketOverviewData && (
                    <CardChart data={data.marketOverviewData} />
                  )}
                </div>
              </div>
            </div>
            <div className="col col-9">
              <div className="card ripple-controls-map-card">
                <div className="card-body">
                  <div className="card-title">
                    <label>Situational Map</label>
                    <div className="ripple-map-distance">
                      <MapDistance />
                    </div>
                    <div className="more-btn">
                      <a>
                        <svg
                          width="4"
                          height="20"
                          viewBox="0 0 4 20"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            fill-rule="evenodd"
                            clip-rule="evenodd"
                            d="M0.0658571 2.35131C0.0658571 3.36445 0.799037 4.18577 1.70346 4.18577C2.60788 4.18577 3.34106 3.36445 3.34106 2.35131C3.34106 1.33816 2.60788 0.516846 1.70346 0.516846C0.799037 0.516846 0.0658571 1.33816 0.0658571 2.35131ZM0.0658568 9.68916C0.0658568 10.7023 0.799037 11.5236 1.70346 11.5236C2.60788 11.5236 3.34106 10.7023 3.34106 9.68917C3.34106 8.67602 2.60788 7.8547 1.70346 7.8547C0.799037 7.8547 0.0658569 8.67602 0.0658568 9.68916Z"
                            fill="#7C7C84"
                          />
                          <ellipse
                            cx="1.70346"
                            cy="17.3581"
                            rx="1.83446"
                            ry="1.6376"
                            transform="rotate(90 1.70346 17.3581)"
                            fill="#7C7C84"
                          />
                          <ellipse
                            cx="1.70346"
                            cy="9.6892"
                            rx="1.83446"
                            ry="1.6376"
                            transform="rotate(90 1.70346 9.6892)"
                            fill="#7C7C84"
                          />
                        </svg>
                      </a>
                    </div>
                  </div>

                  <div className="ripple-map-controls-wrapper">
                    <div className="ripple-map-controls">
                      <MapControl />
                    </div>
                    <div className="card-content ripple-chart">
                      {map && (
                        <div ref={mapContainer} className="map-container" />
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="row market-growth-occupation-row">
            <div className="col col-3">
              <div className="card market-distribution-card">
                <div className="card-body">
                  <div className="card-title">
                    <label for="">Market Age Distribution</label>
                    <span className="sub-text">Aging Labor Ratio: 1.71</span>
                  </div>
                  <div className="card-content">
                    <BarChart data={data.marketAgeDistributionData} />
                  </div>
                </div>
              </div>
            </div>
            <div className="col col-9">
              <div className="row">
                <div className="col col-4">
                  <div className="card occupational-overview-card">
                    <div className="card-body">
                      <div className="card-title">
                        <label for="">Occupational Overview</label>
                        <div className="icon">
                          <span>
                            <svg
                              width="24"
                              height="24"
                              viewBox="0 0 24 24"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <path
                                d="M7.95 10.05L10.05 7.95L5.55 3.45L7.5 1.5H1.5V7.5L3.45 5.55L7.95 10.05Z"
                                fill="#175186"
                              />
                              <path
                                d="M10.05 16.0492L7.95 13.9492L3.45 18.4492L1.5 16.4992V22.4992H7.5L5.55 20.5492L10.05 16.0492Z"
                                fill="#175186"
                              />
                              <path
                                d="M16.05 13.9492L13.95 16.0492L18.45 20.5492L16.5 22.4992H22.4999V16.4992L20.5499 18.4492L16.05 13.9492Z"
                                fill="#175186"
                              />
                              <path
                                d="M16.5 1.5L18.45 3.45L13.95 7.95L16.05 10.05L20.5499 5.55L22.4999 7.5V1.5H16.5Z"
                                fill="#175186"
                              />
                            </svg>
                          </span>
                        </div>
                        {/* <span className="sub-text">Aging Labor Ratio: 1.71</span> */}
                      </div>
                      <div className="card-content">
                        <div className="chart-price">
                          <h4 class="dynamic-text">
                            Lawyers<span className="up">▲10%</span>
                          </h4>
                        </div>
                        <div className="summary">
                          <label for="">Compared to $21,490 last year</label>
                        </div>

                        <div className="occupational-overview-progress">
                          <ul>
                            {data.marketOccupationOverviewData &&
                              data.marketOccupationOverviewData.map((val) => (
                                <li key={val.name}>
                                  <span className="name">{val.name}</span>
                                  <span className="value">
                                    {parseFloat(val.value).toFixed(2)}
                                  </span>
                                </li>
                              ))}
                          </ul>
                          <div className="progress-wrapper">
                            <div className="progress-bar">
                              <div
                                className="progress"
                                style={{
                                  backgroundColor: "#81cab2",
                                  width: "20%",
                                }}
                              ></div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col col-4">
                  <div className="card market-education-card">
                    <div className="card-body">
                      <div className="card-title">
                        <label for="">Market Education Attainment</label>
                        {/* <span className="sub-text">Aging Labor Ratio: 1.71</span> */}
                      </div>
                      <div className="card-content">
                        <RedialChart data={data.marketEducationData} />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col col-4">
                  <div className="card market-race-distribution-card">
                    <div className="card-body">
                      <div className="card-title">
                        <label for="">Market Race Distribution</label>
                        <span className="sub-text">
                          30.43% of population is Hispanic (Ethnicity)
                        </span>
                      </div>
                      <div className="card-content">
                        <RedialChart2 data={data.marketRaceDistributionData} />
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col col-12">
                  <div className="card growth-lawyers-card">
                    <div className="card-body">
                      <div className="card-title">
                        <label class="dynamic-label" for="">
                          Growth of Lawyers
                        </label>
                        {/* <span className="sub-text">30.43% of population is Hispanic (Ethnicity)</span> */}

                        <div className="map-control-wrapper d-none">
                          <div className="map-control-item">
                            <ul>
                              <li>
                                <a>
                                  <svg
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                  >
                                    <path
                                      d="M11 11V5H13V11H19V13H13V19H11V13H5V11H11Z"
                                      fill="#5A5A89"
                                    ></path>
                                  </svg>
                                </a>
                              </li>
                            </ul>
                          </div>
                          <div className="map-control-item">
                            <ul>
                              <li>
                                <a>
                                  <svg
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                  >
                                    <path
                                      d="M11 11H13H19V13H13H11H5V11H11Z"
                                      fill="#5A5A89"
                                    ></path>
                                  </svg>
                                </a>
                              </li>
                            </ul>
                          </div>
                          <div className="map-control-item">
                            <ul>
                              <li>
                                <a>
                                  <svg
                                    width="20"
                                    height="20"
                                    viewBox="0 0 20 20"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                  >
                                    <path
                                      d="M16 9V6L20 10L16 14V11H11V16H14L10 20L6 16H9V11H4V14L0 10L4 6V9H9V4H6L10 0L14 4H11V9H16Z"
                                      fill="#5A5A89"
                                    ></path>
                                  </svg>
                                </a>
                              </li>
                            </ul>
                          </div>
                        </div>
                        <div class="more-btn">
                          <a>
                            <svg
                              width="4"
                              height="20"
                              viewBox="0 0 4 20"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <path
                                fill-rule="evenodd"
                                clip-rule="evenodd"
                                d="M0.0658571 2.35131C0.0658571 3.36445 0.799037 4.18577 1.70346 4.18577C2.60788 4.18577 3.34106 3.36445 3.34106 2.35131C3.34106 1.33816 2.60788 0.516846 1.70346 0.516846C0.799037 0.516846 0.0658571 1.33816 0.0658571 2.35131ZM0.0658568 9.68916C0.0658568 10.7023 0.799037 11.5236 1.70346 11.5236C2.60788 11.5236 3.34106 10.7023 3.34106 9.68917C3.34106 8.67602 2.60788 7.8547 1.70346 7.8547C0.799037 7.8547 0.0658569 8.67602 0.0658568 9.68916Z"
                                fill="#7C7C84"
                              ></path>
                              <ellipse
                                cx="1.70346"
                                cy="17.3581"
                                rx="1.83446"
                                ry="1.6376"
                                transform="rotate(90 1.70346 17.3581)"
                                fill="#7C7C84"
                              ></ellipse>
                              <ellipse
                                cx="1.70346"
                                cy="9.6892"
                                rx="1.83446"
                                ry="1.6376"
                                transform="rotate(90 1.70346 9.6892)"
                                fill="#7C7C84"
                              ></ellipse>
                            </svg>
                          </a>
                        </div>
                      </div>
                      <div className="card-content">
                        <LineChart data={data.marketOccupationGrowthData} />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="row living-wage-calc-row">
            <div className="col col-12">
              <div className="card analysis-column-card">
                <div className="card-body">
                  <div className="card-title">
                    <label htmlFor="">Living Wage Calculator</label>
                    <div className="map-distance-wrapper">
                      <span className="label">Wage:</span>
                      <ul>
                        <li className="active">
                          <a>Hourly</a>
                        </li>
                        <li>
                          <a>Monthly</a>
                        </li>
                        <li>
                          <a>Annually</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div className="card-content">
                    <ColumnChart data={data.marketOccupationWagesData} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

const mapStateToProps = createStructuredSelector({
  theme: selectThemeSetting,
});

const mapDispatchToProps = (dispatch) => ({
  setThemeSetting: (theme) => dispatch(setThemeSetting(theme)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CowBoys);
