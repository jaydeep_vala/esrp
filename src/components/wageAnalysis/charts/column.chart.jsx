import React, { useEffect, useState } from 'react';
import Chart from 'react-apexcharts';

const dataSet2 = {
  tooltip: {
    enabled: true,
  },
  colors: ['#81CAB2', '#5C86C1'],
  chart: {
    stacked: true,
    toolbar: {
      show: false,
    },
    zoom: {
      enabled: false,
    },
  },
  legend: {
    show: false,
  },
  dataLabels: {
    enabled: false,
  },
  grid: {
    show: false,
  },
  plotOptions: {
    bar: {
      // horizontal: false,
      columnWidth: '10%',
      borderRadius: 7,
      ranges: [
        {
          from: 20,
          to: 100,
          color: '#F15B46',
        },
        {
          from: 0,
          to: 20,
          color: '#FEB019',
        },
      ],
    },
  },
  xaxis: {
    // show: false,
    labels: {
      show: true,
    },
    lines: {
      show: false,
    },
    axisTicks: {
      show: false,
    },
  },
  yaxis: {
    labels: {
      show: true,
    },
    lines: {
      show: false,
    },
  },
  annotations: {
    yaxis: [
      {
        y: 10,
        borderColor: '#00E396',
        label: {
          borderColor: '#00E396',
          style: {
            color: '#fff',
            background: '#00E396',
          },
        },
      },
    ],
  },
};

const defaultSeries = [
  {
    name: 'Living Wage',
    data: [],
  },
];

const ColumnChart = ({ data, employeeWage = 0 }) => {
  const [options, setOptions] = useState(dataSet2);
  const [series, setSeries] = useState(defaultSeries);

  useEffect(() => {
    if (data) {
      if (data.label) {
        let categoriesLabel = [];
        for (var i = 0; i < data.label.length; i++) {
          var array = data.label[i].split(' ');
          let temp = [];
          for (var j = 0; j < array.length; j += 2) {
            temp.push(array[j] + ' ' + array[j + 1]);
          }
          categoriesLabel.push(temp);
        }
        setOptions({
          ...options,
          xaxis: { ...options.xaxis, categories: categoriesLabel },
          annotations: {
            yaxis: [{ ...dataSet2.annotations.yaxis[0], y: employeeWage }],
          },
        });
      }
      if (data.series) {
        setSeries([
          {
            data: data.series,
          },
        ]);
      }
    }
  }, [data]);

  return (
    <>
      {series && (
        <Chart
          options={options}
          series={series}
          type='bar'
          width='100%'
          height='450px'
        />
      )}
    </>
  );
};
export default ColumnChart;
