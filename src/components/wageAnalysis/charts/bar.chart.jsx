import React, { useEffect, useState } from 'react';
import Chart from 'react-apexcharts';

const dataSet = {
  chart: {
    type: 'bar',
    toolbar: {
      show: false,
    },
  },
  labels: ['Apples', 'Oranges', 'Berries'],
  plotOptions: {
    bar: {
      borderRadius: 18,
      barHeight: '80px',
      distributed: true,
      horizontal: true,
      dataLabels: {
        position: 'bottom',
      },
    },
  },
  colors: ['#5C86C1', '#3FB7F3', '#81CAB2'],
  dataLabels: {
    enabled: true,
    textAnchor: 'start',
    style: {
      colors: ['#fff'],
    },
    formatter: function (val, opt) {
      var dataPoint = val;
      return dataPoint >= 1000
        ? '$' + dataPoint.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
        : '$' + dataPoint;
    },
    offsetX: 0,
    dropShadow: {
      enabled: false,
    },
  },
  stroke: {
    width: 0,
    colors: ['#fff'],
  },
  xaxis: {
    labels: {
      show: false,
    },
    categories: [
      'Required Annual Income Before Taxes',
      'Required Annual Income After Taxes',
      'Annual Taxes',
    ],
    axisBorder: {
      show: true,
      color: '#78909C',
      height: 1,
      width: '100%',
      offsetX: 0,
      offsetY: 0,
    },
  },
  yaxis: {
    labels: {
      show: false,
    },
  },
  grid: {
    show: true,
    borderColor: '#90A4AE',
    strokeDashArray: 0,
    position: 'back',
    xaxis: {
      lines: {
        show: false,
      },
    },
    yaxis: {
      lines: {
        show: false,
      },
    },
  },
  tooltip: {
    theme: 'light',
    custom: function ({ series, seriesIndex, dataPointIndex, w }) {
      var dataPoint = series[seriesIndex][dataPointIndex];
      return dataPoint >= 1000
        ? '$' + dataPoint.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
        : '$' + dataPoint;
    },
  },
};

const defaultSeries = [
  {
    data: [],
  },
];

const BarChart = ({ data }) => {
  const [options, setOptions] = useState(dataSet);
  const [series, setSeries] = useState(defaultSeries);
  useEffect(() => {
    if (data) {
      if (data.label) {
        setOptions({
          ...options,
          xaxis: { ...options.xaxis, categories: data.label },
        });
      }
      if (data.series) {
        setSeries([
          {
            data: data.series,
          },
        ]);
      }
    }
  }, [data]);

  return (
    <>
      {series && (
        <Chart
          options={options}
          series={series}
          type='bar'
          width='100%'
          height='250'
        />
      )}
    </>
  );
};
export default BarChart;
