import React, { useRef, useEffect, useState } from "react";
import { connect } from "react-redux";
import Chart from "react-apexcharts";
import Select from "../control-component/selectbox";
import "./LaborSupply.style.scss";

const LaborSupply = () => {
  const [segmentationWeight, setSegmentationWeight] = useState();
  const [laborWeight, setLaborWeight] = useState();

  // FOR BAR CHART
  const ActivelyEmployed = {
    series: [
      {
        data: [746165, 393555, 234881, 201881, 103481, 70000],
      },
    ],
    options: {
      chart: {
        type: "bar",
        toolbar: {
          show: false,
        },
      },
      labels: ["Apples", "Oranges", "Berries"],
      plotOptions: {
        bar: {
          borderRadius: 0,
          barHeight: "70%",
          distributed: true,
          horizontal: true,
          dataLabels: {
            position: "bottom",
          },
        },
      },
      legend: {
        show: false,
      },
      colors: ["#5C86C1", "#3FB7F3", "#81CAB2"],
      dataLabels: {
        enabled: true,
        textAnchor: "start",
        style: {
          colors: ["#fff"],
        },
        formatter: function (val, opt) {
          var amount = parseInt(val);
          if (amount > 1000 && amount < 230000) {
            amount = amount / 1000;
            amount = Math.round(amount).toString();
            amount = amount + "k";
          } else {
            amount =
              amount >= 1000
                ? amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                : amount;
          }

          return amount;
        },
        offsetX: 0,
        dropShadow: {
          enabled: false,
        },
      },
      stroke: {
        width: 0,
        colors: ["#fff"],
      },
      xaxis: {
        labels: {
          show: true,
          formatter: function (val, opt) {
            var amount = parseInt(val);
            if (amount > 1000 && amount > 150000) {
              amount = amount / 1000;
              amount = Math.round(amount).toString();
              amount = amount + "k";
            }
            return amount;
          },
        },
        categories: [
          "Los Angeles, CA",
          "Phoenix, AZ",
          "Philadelphia, PA",
          "Las Vegas, NV",
          "Salt Lake City, NV",
          "Reno, NV",
        ],
        axisBorder: {
          show: false,
          color: "#78909C",
          height: 1,
          width: "100%",
          offsetX: 0,
          offsetY: 0,
        },
        axisTicks: {
          show: false,
        },
      },
      yaxis: {
        labels: {
          show: true,
        },
        axisBorder: {
          show: false,
        },
      },
      grid: {
        show: true,
        borderColor: "#E6E8ED",
        strokeDashArray: 0,
        position: "back",
        xaxis: {
          lines: {
            show: false,
          },
        },
        yaxis: {
          lines: {
            show: true,
          },
        },
      },
      tooltip: {
        theme: "light",
        custom: function ({ series, seriesIndex, dataPointIndex, w }) {
          var dataPoint = series[seriesIndex][dataPointIndex];
          return dataPoint >= 1000
            ? dataPoint.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
            : dataPoint;
        },
      },
    },
  };

  return (
    <>
      <section className="labor-supply-sec">
        <div className="container">
          <div className="row title-filter-row">
            <div className="col col-6">
              <div className="breadcrumbs">
                <ul>
                  <li className="breadcrumb-item">
                    <a href="#!">Home</a>
                  </li>
                  <li className="breadcrumb-item">
                    <a href="#!">Dashboards</a>
                  </li>
                  <li className="breadcrumb-item">MSA Ranking</li>
                </ul>
              </div>
              <h3 className="page-title">Labor Supply</h3>
            </div>
            <div className="col col-6">
              <div className="analysis-filter-wrapper">
                <div className="analysis-filter-item">
                  <button className="btn rounded outline">Download</button>
                </div>
                <div className="analysis-filter-item labor-supply-weight">
                  <Select
                    selectItem={laborWeight}
                    setSelectedItem={setLaborWeight}
                    lableName="Labor Supply Weight"
                    options={["10", "20", "30", "40", "50", "50"]}
                    // selectIcon={
                    //   <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    //     <path d="M21 10C21 17 12 23 12 23C12 23 3 17 3 10C3 7.61305 3.94821 5.32387 5.63604 3.63604C7.32387 1.94821 9.61305 1 12 1C14.3869 1 16.6761 1.94821 18.364 3.63604C20.0518 5.32387 21 7.61305 21 10Z" stroke="#5C86C1" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                    //     <path d="M12 13C13.6569 13 15 11.6569 15 10C15 8.34315 13.6569 7 12 7C10.3431 7 9 8.34315 9 10C9 11.6569 10.3431 13 12 13Z" stroke="#5C86C1" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                    //   </svg>
                    // }
                  />
                </div>
                <div className="analysis-filter-item segmentation-weight">
                  <Select
                    selectItem={segmentationWeight}
                    setSelectedItem={setSegmentationWeight}
                    lableName="Segmentation Weighting"
                    options={["10", "20", "30", "40", "50", "50"]}
                    // selectIcon={
                    //   <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    //     <path d="M21 10C21 17 12 23 12 23C12 23 3 17 3 10C3 7.61305 3.94821 5.32387 5.63604 3.63604C7.32387 1.94821 9.61305 1 12 1C14.3869 1 16.6761 1.94821 18.364 3.63604C20.0518 5.32387 21 7.61305 21 10Z" stroke="#5C86C1" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                    //     <path d="M12 13C13.6569 13 15 11.6569 15 10C15 8.34315 13.6569 7 12 7C10.3431 7 9 8.34315 9 10C9 11.6569 10.3431 13 12 13Z" stroke="#5C86C1" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                    //   </svg>
                    // }
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="row chart-row">
            <div className="col col-6">
              <div className="card actively-employed-card labor-supply-card">
                <div className="card-body">
                  <div className="card-title">
                    <label htmlFor="">Number of</label>
                    <span className="big-text">Actively Employed Workers</span>
                  </div>
                  <div className="card-content">
                    <div className="chart-wrapper">
                      <Chart
                        options={ActivelyEmployed.options}
                        series={ActivelyEmployed.series}
                        type="bar"
                        width="100%"
                        height="510"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col col-6">
              <div className="card households-profile-card labor-supply-card">
                <div className="card-body">
                  <div className="card-title">
                    <label htmlFor="">Number of</label>
                    <span className="big-text">
                      Households with Industrial Mosaic Profiles
                    </span>
                  </div>
                  <div className="card-content">
                    <div className="chart-wrapper">
                      <Chart
                        options={ActivelyEmployed.options}
                        series={ActivelyEmployed.series}
                        type="bar"
                        width="100%"
                        height="510"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default LaborSupply;
