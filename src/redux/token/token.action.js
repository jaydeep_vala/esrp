import TokenActionTypes from './token.type';

export const setToken = (token) => ({
  type: TokenActionTypes.SET_TOKEN,
  payload: token
})