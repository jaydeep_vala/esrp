import { createSelector } from "reselect";

const selecttoken = (state) => state.token;

export const selectToken = createSelector([selecttoken], (token) => token);
