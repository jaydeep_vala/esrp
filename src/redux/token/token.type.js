const TokenActionTypes = {
  SET_TOKEN: 'SET_TOKEN'
};

export default TokenActionTypes;