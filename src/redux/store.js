import { createStore, applyMiddleware } from "redux";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import logger from "redux-logger";
import rootReducer from "./root.reducer";

const Middleware = [];

if (process.env.NODE_ENV === "development") {
  Middleware.push(logger);
}

const persistConfig = {
  key: "esrp",
  storage: storage,
  blackList: ["user"],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = createStore(
  persistedReducer,
  applyMiddleware(...Middleware)
);

export const persistor = persistStore(store);

const exportStore = { store, persistor };

export default exportStore;
