import React, { useState } from "react";
import "./login.styles.scss";
import Input from "../../components/control-component/input";
import { useHistory } from "react-router-dom";
import { connect } from "react-redux";
import { setCurrentUser } from "../../redux/user/user.action";
import { setToken } from "../../redux/token/token.action";
import { login } from "../../services/authentication.service";

const Login = ({ setToken, setCurrentUser }) => {
  let history = useHistory();
  const [values, setValues] = useState({
    email: "",
    password: "",
  });
  const [buttonEvent, setButtonEvent] = useState(false);
  const [loginError, setLoginError] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);

  // useEffect(() => {
  //   setToken(null);
  //   setCurrentUser(null);
  // }, []);

  const handleChange = (event, key) => {
    const temp = { ...values };
    temp[key] = event.target.value;
    setValues(temp);
  };

  const submitEvent = (event) => {
    event.preventDefault();
    setButtonEvent(true);
    const user = {
      email: values.email,
      password: values.password,
    };
    login(user).then(async (res) => {
      setButtonEvent(false);
      if (res.status !== "error") {
        if (res.data && res.data !== null && res.data !== "") {
          setToken(res.data.token);
          setCurrentUser(res.data.user);
          history.push("/");
        }
      } else {
        if (res.message) {
          setLoginError(true);
          if (typeof res.message === "string") {
            setErrorMessage(res.message);
          }
          if (res.message.email) {
            setErrorMessage(res.message.email);
          }
          if (res.message.password) {
            setErrorMessage(res.message.password);
          }
        }
      }
    });
  };
  return (
    <>
      <section className="login-sec">
        <div className="bg-pattern">
          <img src="assets/images/login-pattern.svg" alt="" />
        </div>
        <div className="container-fluid">
          <div className="row">
            <div className="col col-3 mx-auto">
              <div className="login-card-wrapper card">
                <div className="card-body">
                  <div className="site-logo-icon">
                    <img src="assets/images/esrp-logo.png" alt="" />
                  </div>
                  <div className="login-text-content">
                    <h3>Login</h3>
                    {/* <p>
                      Lorem ipsum dolor sit amet consectetur adipisicing elit.
                      Adipisci velit quas quia nulla. Itaque!
                    </p> */}
                  </div>
                  {loginError ? (
                    <span style={{ color: "red" }}> {errorMessage}</span>
                  ) : null}
                  <form onSubmit={submitEvent}>
                    <div className="form-row" noValidate name="loginForm">
                      <Input
                        name="email"
                        type="email"
                        lable="Email"
                        value={values.email}
                        handleChange={(e) => handleChange(e, "email")}
                      />
                      <Input
                        name="password"
                        type="password"
                        lable="Password"
                        value={values.password}
                        handleChange={(e) => handleChange(e, "password")}
                      />
                      <div className="input-item forget">
                        <span>
                          Forgot Password? <a href="#!">Reset here</a>
                        </span>
                      </div>
                      <div className="input-item">
                        <button
                          type="submit"
                          className="btn primary rounded"
                          disabled={buttonEvent}
                        >
                          Login
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

const mapDispatchToProps = (dispatch) => ({
  setToken: (token) => dispatch(setToken(token)),
  setCurrentUser: (user) => dispatch(setCurrentUser(user)),
});

export default connect(null, mapDispatchToProps)(Login);
